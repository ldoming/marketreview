<?php
/*
Plugin Name: Featured Reviews
Plugin URI: http://ldoming.webuda.com
Description: This is for the featured review
Author: Lymuel Doming
Version: 1.0
Author URI: http://ldoming.webuda.com
*/


add_action('admin_menu',function(){
		add_options_page( 'Featured Reviews', 'Featured Reviews', 'manage_options', __FILE__, 'featured_reviews_menu_admin');
	}
);


function featured_reviews_menu_admin() {


	if ($_POST['submit_form_featured_reviews']){
		$reviews = array();
		$data = array_keys($_POST);
		for($loop = 0; $loop < sizeof($data) -1 ; $loop++){
			$reviews[] = $data[$loop];
		}
		update_option('pr_custom_featured_reviews', $reviews);
	}

	$pr_custom_featured_reviews = (get_option('pr_custom_featured_reviews')) ? get_option('pr_custom_featured_reviews') : array();
	?>

	<script type="text/javascript">
		function checkboxlimit(checkgroup, limit){
			var checkgroup=checkgroup
			var limit=limit
			for (var i=0; i<checkgroup.length; i++){
				checkgroup[i].onclick=function(){
				var checkedcount=0
				for (var i=0; i<checkgroup.length; i++)
					checkedcount+=(checkgroup[i].checked)? 1 : 0
				if (checkedcount>limit){
					this.checked=false
					}
				}
			}
		}
	</script>

	<div class="wrap">
		<h1>Featured Reviews</h1>
		<h4>Please check 5 reviews to be displayed in the home page</h4>
		<form action="" method="POST" name="featuredReviews">
		<table class="widefat">
			<thead>
				<tr>
					<th>Image Preload</th>
					<th>Title</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
					global $wpdb;

					$reviews = $wpdb->get_results(
						"
							SELECT `ID`,`post_title`, `post_content`
							FROM $wpdb->posts
							WHERE `post_type` = 'reviews'
								AND `post_status` = 'publish'
						"
					);
					foreach ($reviews as $review) {
				?>

				<tr>
					<td width="15%">
					<?php 
					$links = wp_get_attachment_image_src( get_post_thumbnail_id($review->ID),"full");
					if( $links[0] ) {
					?>
                	<a class="<?php echo $class; ?>" href="<?php echo $review_image_link; ?>">
                		<img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo wp_get_attachment_url( get_post_thumbnail_id($review->ID) ); ?>&w=<?php echo get_option('pr_thumb_width'); ?>&h=<?php echo get_option('pr_thumb_height'); ?>" alt="single_review" />
                	</a>
                   <?php } else { ?>
               		<a class="<?php echo $class; ?>" href="<?php echo $review_image_link; ?>">
                		<img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo get_template_directory_uri();?>/images/noimg-280.png&w=<?php echo get_option('pr_thumb_width'); ?>&h=<?php echo get_option('pr_thumb_height'); ?>" alt="single_review" />
               		</a>
                   <?php } ?>
					</td>
					<td width="15%"><?php echo $review->post_title;?></td>
					<td width="30%"><?php echo description_excerpt(200,$review->post_content,'...')?></td><!-- <td width="65%"><?php echo $review->post_content;?></td> -->
					<td width="15%">
						<input type="checkbox" name='<?php echo $review->ID; ?>' <?php echo (array_search($review->ID, $pr_custom_featured_reviews) !== false) ? 'checked = "cheked"' : ""; ?> >
					</td>
				</tr>

				<?php
					}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th>Image Preload</th>
					<th>Title</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</tfoot>
		</table>
		<input type="submit" class="default-primary" name="submit_form_featured_reviews" value="Submit">
		</form>
	</div>
	<script type="text/javascript">
		checkboxlimit(document.forms.featuredReviews, 5)
	</script>
	<?php
}

function description_excerpt($char_size = null, $description = null, $end_option = null){
	if ($char_size == null) { return $description;}
	if ($description == null) { return;}
	$description = substr($description, 0, $char_size);
	$description .= $end_option;
	return $description;
}


?>
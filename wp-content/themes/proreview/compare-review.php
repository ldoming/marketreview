<?php
/***
Template Name: Compare Review
***/
?>
<?php get_header(); ?>        
    

<div class="main_content padd">
    <div id="primary">       
        
        <div class="standard_post_items"> 
			<table width="100%" id="compare_review_table"  class="tablesorter" >
				<thead>
				<tr>
					<th><?php _e("Review Title",'prt');?></th>
					<th><?php _e("Description",'prt');?></th>
					<th><?php _e("Rating",'prt');?></th>
					<th><?php _e("Price",'prt');?></th>
				</tr>
				</thead>
				<tbody> 
					<?php   
						$temp = $wp_query;
						$wp_query= null;
						$wp_query = new WP_Query();
						global $custom_metabox;
						
						$wp_query->query('posts_per_page=-1&post_type=reviews');
						if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); 
						$meta = get_post_meta(get_the_ID(), $custom_metabox->get_the_ID(), TRUE); 
						if( $meta['r_rating'] == "" && get_post_meta(get_the_ID(), "r_rating",TRUE) != "" ) $meta['r_rating'] 	= get_post_meta(get_the_ID(), "r_rating",TRUE);
						if( $meta['r_price'] == "" && get_post_meta(get_the_ID(), "r_price",TRUE) != "" ) $meta['r_price'] 		= get_post_meta(get_the_ID(), "r_price",TRUE);
					?> 
					
					<?php if( $i%2==0 ) $class = "odd"; else $class = "even"; ?>    
				  <tr  class="<?php echo $class;?>">
				   <td><h6 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6></td>
				   <td><?php echo theme_excerpt('480',__('Read More...',"prt")); ?></td> 
				   <td><div class="review_rating stars_<?php echo $meta['r_rating']; ?>"><?php echo $meta['r_rating']; ?><?php _e("stars",'prt');?></div></td>
				   <td><?php echo $meta['r_price']; ?></td>         
				   </tr>         
			<?php $i++; endwhile; endif; wp_reset_query(); ?>
			</tbody>
			</table>            
        </div><!--end standard_post_items-->
        </div><!--end primary-->
        
			<?php get_sidebar(); ?>

        </div><!--end main_content-->
        
<?php get_footer(); ?>
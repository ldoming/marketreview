<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<!--Meta Tags-->
	<meta name="author" content="atinder" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>" />
    <?php if ( get_option('pr_font') == "Futura" ) { ?>
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/fonts/Futura/stylesheet.css" />
    	<style type="text/css">body{font-family: 'FuturaBkBTBook';}</style>    
    <?php } else if( get_option('pr_font') == "gothic" ) { ?>
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/fonts/gothic/fonts.css" />
    <style type="text/css">body{font-family:'Conv_GOTHIC',Sans-Serif;}</style> 
    <?php } else { ?>
    	<style type="text/css">body{font-family:Arila,Helvetica,Sens-Seriff;}</style> 
    <?php } ?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.cycle.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/superfish.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.rating.pack.js"></script>

    
    <link rel="shortcut icon" href="<?php if(get_option('pr_custom_favicon')){echo get_option('pr_custom_favicon');} ?>" />
		<title><?php if (is_home()) {	echo bloginfo('name');} elseif (is_404()) {	echo '404 Not Found';} elseif (is_category()) {	echo 'Category:'; wp_title('');} elseif (is_search()) {	echo 'Search Results';} elseif ( is_day() || is_month() || is_year() ) {	echo 'Archives:'; wp_title('');} else {	echo wp_title('');}?></title>

<?php if(get_option('pr_images')){ ?>
<style type="text/css">
body{
    background-image:url(<?php echo get_template_directory_uri();?>/images/backgrounds/<?php echo get_option('pr_images') ;?>);
    <?php if(get_option('pr_images')!='default.jpg' && get_option('pr_images')!='wood-horizontal.jpg' && get_option('pr_images')!='wood-dark.jpg' && get_option('pr_images')!='dark-grain.jpg' && get_option('pr_images')!='mosaic-blue.jpg' && get_option('pr_images')!='mosaic-red.jpg' && get_option('pr_images')!='white.jpg' && get_option('pr_images')!='mosaic-black.jpg') {?>
    background-repeat: repeat-x;
    <?php } ?>
}
<?php if(get_option('pr_header_bg') || get_option('pr_header_cbg')){ ?>
#header{
    background-image:url(<?php if(get_option('pr_header_cbg')){echo get_option('pr_header_cbg');}else{ echo get_template_directory_uri();?>/images/backgrounds/<?php if(get_option('pr_header_bg')){ echo get_option('pr_header_bg'); }} ;?>);
    background-repeat: repeat-x;
}
<?php } ?>

<?php if(is_page() or is_single()){
	?>
#primary a
{
	color: #3366BB !important;
}
	<?php
}?>
</style>
<?php } ?>

<style type="text/css">
<?php if (is_home()) :?>
#header{
  margin-top:92px;
}
.page_wrapper {
  margin-top: 200px;
}
<?php else :?>
#header h1.logo img{
    float: left;
    width:127px;
}
<?php endif;?>

.main_content.padd, .main_content.blog {
  padding:0px;
}
.main_content {
  padding:0px;
}
#primary{
  width: 100%;
}
.single_review {
  margin-bottom: 0px;
}
.page_wrapper {
}





</style>



<?php if(get_option('pr_custom_css')){ ?>
<style type="text/css">
<?php echo get_option('pr_custom_css') ;?>
</style>
<?php } ?>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>  
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=156143837823402";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div style="border-top:5px solid #037979;">
    <div style="width:960px;margin:0 auto;">
        <span class="contact-us">
            Contact Us
        </span>
    </div>
</div>

<div id="body_wrapper">
    <div class="top_list">
    
        <div class="top_list_in">
        
		<?php if ( has_nav_menu( 'top-menu' ) ) { 
		  wp_nav_menu( array( 'theme_location' => 'top-menu','menu_class'=>'sf-js-enabled sf-shadow','depth'=>'0' ) ); 
        ?>
        <a class="subscribe" href="<?php echo get_option('pr_rss_link'); ?>">Subscribe</a>
        <?php } ?>
    </div><!--end top_list_in-->
    </div><!--end top_list-->
    
    
        <div id="header">
            <?php if ( get_option( 'pr_logo' ) != "" ) : ?>
              <h1 class="logo">
                <a href="<?php echo home_url(); ?>">
                  <img src="<?php echo get_option('pr_logo'); ?>" alt="<?php bloginfo('name'); ?>" />
                </a>
              </h1>
            <?php 
              endif;
              if (is_home()) :
                if (get_option( 'pr_header_text' ) == "true") :
            ?>
              <p style="text-align:center;font-weight:bold;font-size:30px;margin-top:30px;padding-bottom:20px;"><?php echo get_option( 'pr_header_top_text' );?></p>
              <p style="text-align:center;font-size:20px;"><?php echo get_option( 'pr_header_top_text_2' );?></p>
            <?php 
                endif; 
              endif;
            ?> 
            <?php if(get_option('pr_top_banner') && get_option('pr_tb_link')){?>
            <div class="header_banner">
            <a href="<?php echo get_option('pr_tb_link'); ?>"><img src="<?php echo get_option('pr_top_banner'); ?>" alt="banner" /></a>
            </div><!--end header_banner-->
             <?php } ?>
        </div><!--end header-->

        <?php include (TEMPLATEPATH . '/includes/functions/featured_reviews.php'); ?>
        
        <div class="page_wrapper">
       <div class="cat_nav">
            <?php if ( has_nav_menu( 'cat-menu' ) ) { wp_nav_menu( array( 'theme_location' => 'cat-menu','menu_class'=>'sf-js-enabled sf-shadow sf-menu','depth'=>'0' ) );} ?>
       <div class="clear"></div>
       </div>
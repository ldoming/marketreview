<?php if(get_option('pr_sidebar') == 'true') : ?>
	<div id="sidebar">
	    <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar')) : ?>
		<div class="widget_area_side">
			<p>
				<?php _e("You can add widgets here from Widgets Page",'prt');?>
			</p>
		</div><!--end widget_area_side -->
		<?php endif; ?>
	</div><!--end sidebar-->
<?php endif; ?>
<?php
/***
Template Name:Blog
***/
?>
<?php get_header(); ?>        
    

<div class="main_content padd">
    <div id="primary">               
        <div class="standard_post_items">
	<?php   
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$temp = $wp_query;
	$wp_query= null;
	$wp_query = new WP_Query();
	$wp_query->query('post_type=post&showposts=10&paged='.$paged);
	$review_id = get_the_ID();
	if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
	?>     
       <div class="post_item">
                <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p class="post_meta"><?php _e("Posted by",'prt');?>: <?php the_author();?>   <?php _e("on",'prt');?> <?php the_time('F j, Y');?>   <?php _e("Under",'prt');?>: <?php echo get_the_term_list( $post->ID, 'category', '', ', ', '' ); ?></p>
                <?php echo theme_excerpt('480',__("Read More",'prt')); ?>
            </div><!--end post_item-->
<?php endwhile; endif;?>            
             
	<?php prt_pagination($wp_query->max_num_pages); ?>   
         
        </div><!--end standard_post_items-->
    </div><!--end primary-->
        
<?php get_sidebar(); ?>

        </div><!--end main_content-->
        
<?php get_footer(); ?>
    $(document).ready(function() { 
        $('ul.sf-menu').superfish({ 
            delay:       600,                            // one second delay on mouseout 
            animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
            speed:       'slow',                          // faster animation speed 
            autoArrows:  true,                           // disable generation of arrow mark-up 
            dropShadows: true                            // disable drop shadows 
        }); 
 
	/* This is basic - uses default settings */
	
	$("a.single_image").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	true
	});
});	

/******Image Preload *******/


(function($){
    $.fn.preloadImages = function(options){

        var defaults = {
            showSpeed: 500,
        };

        var options = $.extend(defaults, options);

        return this.each(function(){
            var container = $(this);
            var image = container.find('img');

            $(image).css({ "visibility": "hidden", "opacity": "0" });
            $(image).bind('load error', function(){
                $(this).css({ "visibility": "visible" }).animate({ opacity:"1" }, {duration:options.showSpeed, easing:options.easing}).parent(container).removeClass('preload');
            }).each(function(){
                if(this.complete || ($.browser.msie && parseInt($.browser.version) == 6)) { $(this).trigger('load'); }
            });
        });
    }
})(jQuery);


jQuery(document).ready(function(){
    jQuery('#body_wrapper').preloadImages({
        showSpeed: 500,   // length of fade-in animation, 500 is default
     });
});
                
$('.star').rating();
$('.star').click(function() {
  $("input#rating").val($(this).text());
  
});
$('div.rating-cancel a').click(function() {
  $("input#rating").val("").css("display","block");
  
});  

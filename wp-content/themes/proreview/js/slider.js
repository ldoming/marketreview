
$slider = {
    context: false,
    tabs: false,
    timeout: 4500,      // time before next slide appears (in ms)
    slideSpeed: 1000,   // time it takes to slide in each slide (in ms)
    tabSpeed: 300,      // time it takes to slide in each slide (in ms) when rotating through tabs
    fx: 'scrollLeft',   // slide control: fade, scrollLeft, right etc.
    
    init: function() {
        // set the context to help speed up selectors/improve performance
        this.context = $('#slider');
        
        // set tabs to current hard coded navigation items
        this.tabs = $('ul.slides-nav li', this.context);
        
        // remove hard coded navigation items from DOM 
        // because they aren't hooked up to jQuery cycle
        this.tabs.remove();
        
        // prepare slider and jQuery cycle tabs
        this.prepareSlideshow();
        
        $('#slider .slides li').css('display','block');
    },
    
    prepareSlideshow: function() {
        // initialise the jquery cycle plugin -
        // for information on the options set below go to: 
        // http://malsup.com/jquery/cycle/options.html
        $('div.slides > ul', $slider.context).cycle({
            fx: $slider.fx,
            timeout: $slider.timeout,
            speed: $slider.slideSpeed,
            fastOnEvent: $slider.tabSpeed,
            pager: $('ul.slides-nav', $slider.context),
            pagerAnchorBuilder: $slider.prepareTabs,
            before: $slider.activateTab,
            pauseOnPagerHover: true,
            pause: true
        });            
    },
    prepareTabs: function(i, slide) {
        // return markup from hardcoded tabs for use as jQuery cycle tabs
        // (attaches necessary jQuery cycle events to tabs)
        return $slider.tabs.eq(i);
    },
    activateTab: function(currentSlide, nextSlide) {
        // get the active tab
        var activeTab = $('a[href="#' + nextSlide.id + '"]', $slider.context);
        
        // if there is an active tab
		if(activeTab.length) {
            // remove active styling from all other tabs
            $slider.tabs.removeClass('on');
            // add active styling to active button
            activeTab.parent().addClass('on');
        }            
    }            
};

$(function() {
    // add a 'js' class to the body
    $('body').addClass('js');    
    // initialise the slider when the DOM is ready
    $slider.init();
});  

<?php
/*
Template Name: Contact
*/
?>


<?php 
//If the form is submitted
if(isset($_POST['submitted'])) {

	//Check to see if the honeypot captcha field was filled in
	if(trim($_POST['checking']) !== '') {
		$captchaError = true;
	} else {
	
		//Check to make sure that the name field is not empty
		if(trim($_POST['contactName']) === '') {
			$nameError = '(Required)';
			$hasError = true;
		} else {
			$name = trim($_POST['contactName']);
		}
		
		//Check to make sure that the subject field is not empty
		if(trim($_POST['contactSubject']) === '') {
			$subjectError = '(Required)';
			$hasError = true;
		} else {
			$subject = trim($_POST['contactSubject']);
		}
		
		//Check to make sure sure that a valid email address is submitted
		if(trim($_POST['email']) === '')  {
			$emailError = '(Required)';
			$hasError = true;
		} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
			$emailError = 'You entered an invalid email address.';
			$hasError = true;
		} else {
			$email = trim($_POST['email']);
		}
			
		//Check to make sure comments were entered	
		if(trim($_POST['comments']) === '') {
			$commentError = '(Required)';
			$hasError = true;
		} else {
			if(function_exists('stripslashes')) {
				$comments = stripslashes(trim($_POST['comments']));
			} else {
				$comments = trim($_POST['comments']);
			}
		}
			
		//If there is no error, send the email
		if(!isset($hasError)) {
if(get_option('pr_email')){ $emailTo = get_option('pr_email'); }
if($subject == NULL){
	if(get_option('pr_email_sub')){ $subject = get_option('pr_email_sub');} else {$subject = 'Contact Form Submission from '. $name; }
}
			echo $subject;
			$sendCopy = trim($_POST['sendCopy']);
			$body = "Name: $name \n\nEmail: $email \n\nComments: $comments";
            $from=get_bloginfo('name');
			$headers = 'From:' . $from . '<'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;
			
			mail($emailTo, $subject, $body, $headers);

			if($sendCopy == true) {
				$subject = 'You emailed Your Name';
				$headers = 'From: Your Name <noreply@somedomain.com>';
				mail($email, $subject, $body, $headers);
			}

			$emailSent = true;

		}
	}
} ?>


<?php get_header(); ?>

<div class="main_content padd">
    <div id="primary">   

<?php if(isset($emailSent) && $emailSent == true) { ?>

	<div id="success" class="thanks">
		<h1><?php e("Thanks",'prt');?>, <?php echo $name;?></h1>
		<p><?php _e("Your email was successfully sent. We will be in touch soon",'prt');?>.</p>
	</div>

<?php } else {
    
        if (have_posts()) : while (have_posts()) : the_post();   
?>          
               
                    
                   <?php the_content(); ?>
                
                
<?php endwhile; endif; ?>

		<?php if(isset($hasError) || isset($captchaError)) { ?>
			<p class="error"><?php _e("There was an error submitting the form",'prt');?>.<p>
		<?php } ?>
	<div id="my_contact_form">
		<form action="<?php the_permalink(); ?>" id="contactForm" method="post">
	
			<ul class="forms">
				<li class="li_name">
					<input type="text" name="contactName" id="contactName" value="<?php if(isset($_POST['contactName'])) echo $_POST['contactName'];?>" class="requiredField" />
					Name*<?php if($nameError != '') { ?>
						<span class="error"><?php echo $nameError;?></span> 
					<?php } ?>
				</li>
				<li class="li_name">
					<input type="text" name="contactSubject" id="contactSubject" value="<?php if(isset($_POST['contactSubject'])) echo $_POST['contactSubject'];?>" class="requiredField" />
					Subject*<?php if($subjectError != '') { ?>
						<span class="error"><?php echo $subjectError;?></span> 
					<?php } ?>
				</li>
				
				<li class="li_email">
					<input type="text" name="email" id="email" value="<?php if(isset($_POST['email']))  echo $_POST['email'];?>" class="requiredField email" />
					Email*<?php if($emailError != '') { ?>
						<span class="error"><?php echo $emailError;?></span>
					<?php } ?>
				</li>
				
				<li class="textarea">
					Your Message
					<textarea name="comments" id="commentsText" rows="10" cols="30" class="requiredField"><?php if(isset($_POST['comments'])) { if(function_exists('stripslashes')) { echo stripslashes($_POST['comments']); } else { echo $_POST['comments']; } } ?></textarea>
					<?php if($commentError != '') { ?>
						<span class="error">Message<?php echo $commentError;?></span> 
					<?php } ?>
				</li>
				<li class="screenReader"><label for="checking" class="screenReader">If you want to submit this form, do not enter anything in this field</label><input type="text" name="checking" id="checking" class="screenReader" value="<?php if(isset($_POST['checking']))  echo $_POST['checking'];?>" /></li>
				<li class="more"><input type="hidden" name="submitted" id="submitted" value="true" /><input class="submit_button" type="submit" value="Submit" /></li>
			</ul>
		</form>
<div class="clear"></div>
</div><!--end my_contact_form -->

<div class="content_sep"></div>            

<?php /*
<div class="map_content">

<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Contact Page Sidebar')) : ?>

<?php endif; ?>  
<div class="clear"></div>
</div>
 
 */ ?>

<?php } ?>
</div><!--end primary_content -->

<div id="sidebar">
<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar')) : ?>

<?php endif; ?>  
</div><!--end sidebar -->


</div><!--end main_content-->
<?php get_footer();?>
<?php get_header(); ?>        
    

<div class="main_content padd">
    <div id="primary">       
<h1 class="title"><?php _e("Page not found", 'prt'); ?></h1>
<p><?php _e("Apologies, but the page you requested could not be found. Perhaps searching will help.", 'prt' ); ?></p>     
        </div><!--end primary-->
        
<?php get_sidebar(); ?>

        </div><!--end main_content-->
        
<?php get_footer(); ?>
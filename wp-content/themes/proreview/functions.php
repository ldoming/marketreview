<?php
/*
function my_fields($fields) {
	$fields['new'] = '<p>Some new input field here</p>';
	return $fields;
	}
add_filter('comment_form_default_fields','my_fields');
*/
/*-----------------------------------------------------------------------------------*/
/* Options Framework Functions
/*-----------------------------------------------------------------------------------*/

/* Set the file path based on whether the Options Framework is in a parent theme or child theme */if ( STYLESHEETPATH == TEMPLATEPATH ) {
	define('OF_FILEPATH', TEMPLATEPATH);
	define('OF_DIRECTORY', get_bloginfo('template_directory'));
} else {
	define('OF_FILEPATH', STYLESHEETPATH);
	define('OF_DIRECTORY', get_stylesheet_directory_uri());
}

/* These files build out the options interface.  Likely won't need to edit these. */

require_once (OF_FILEPATH . '/admin/admin-functions.php');		// Custom functions and plugins
require_once (OF_FILEPATH . '/admin/admin-interface.php');		// Admin Interfaces (options,framework, seo)

/* These files build out the theme specific options and associated functions. */

require_once (OF_FILEPATH . '/admin/theme-options.php'); 		// Options panel settings and custom settings
require_once (OF_FILEPATH . '/admin/theme-functions.php'); 	    // Theme actions based on options settings

/*-----------------------------------------------------------------------------------*/
/* End Options Framework Functions
/*-----------------------------------------------------------------------------------*/

include(TEMPLATEPATH . '/includes/functions/sidebars.php'); 
include(TEMPLATEPATH . '/includes/widgets.php'); 
include(TEMPLATEPATH . '/includes/functions/shortcodes.php');
include(TEMPLATEPATH . '/includes/functions/custom-post-types.php');
include(TEMPLATEPATH . '/includes/functions/menus.php');
include(TEMPLATEPATH . '/includes/functions/custom_metaboxes.php');
include(TEMPLATEPATH . '/includes/functions/custom_functions.php');
include(TEMPLATEPATH . '/includes/functions/core_functions.php');
include(TEMPLATEPATH . '/includes/functions/create-reviews-post.php');
include(TEMPLATEPATH . '/includes/functions/convert-posts-reviews.php');
load_theme_textdomain( 'prt', TEMPLATEPATH.'/languages' );
$locale = get_locale();
$locale_file = TEMPLATEPATH."/languages/$locale.php";
if ( is_readable($locale_file) )
	require_once($locale_file);


add_filter('get_comments_number', 'comment_count', 0);
function comment_count( $count ) {
if ( ! is_admin() ) {
global $id;
$comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
return count($comments_by_type['comment']);
} else {
return $count;
}
} 


function average_rating() {
    global $wpdb;
    $post_id = get_the_ID();
    $ratings = $wpdb->get_results("
        SELECT $wpdb->commentmeta.meta_value
        FROM $wpdb->commentmeta
        INNER JOIN $wpdb->comments on $wpdb->comments.comment_id=$wpdb->commentmeta.comment_id
        WHERE $wpdb->commentmeta.meta_key='rating' 
        AND $wpdb->comments.comment_post_id=$post_id 
        AND $wpdb->comments.comment_approved =1
        ");
    $counter = 0;
    $average_rating = 0;    
    if ($ratings) {
        foreach ($ratings as $rating) {
            $average_rating = $average_rating + $rating->meta_value;
            $counter++;
        } 
        //round the average to the nearast 1/2 point
        return (round(($average_rating/$counter)*2,0)/2);  
    } else {
        //no ratings
        return '0';
    }
}


/*
$file_dir2=get_bloginfo('template_directory');

wp_enqueue_script("searchbox", $file_dir2 ."/js/searchbox.js", false, "1.0",true);

$search_text=get_option('zx_search_text');
$params2= array(
  'search_text' => $search_text,
);

wp_localize_script( 'searchbox', 'my_params2', $params2 );

remove_filter('comment_text', 'wptexturize');

*/


function enque_tablesorter() {
    wp_register_script( 'tablesorter', get_template_directory_uri().'/js/jquery.tablesorter.js', array('jquery'));
    wp_enqueue_script( 'tablesorter' );
}    
 
add_action('wp_enqueue_scripts', 'enque_tablesorter');

/*-----------------------------------------------------------------------------------*/
/* Shortcode Generator Functions
/*-----------------------------------------------------------------------------------*/

require_once (TEMPLATEPATH . '/includes/functions/tinymceModules/tinymce.php');

/*-----------------------------------------------------------------------------------*/
/* Google Plus Button
/*-----------------------------------------------------------------------------------*/

function pro_header_codes(){ ?>
 
   <!-- slider initialization goes here -->
   <?php $slider_control = ( get_option('pr_slider_control') != "" )?get_option('pr_slider_control'):"scrollLeft";
	  $slide_speed = (int)get_option('pr_slide_speed');
	  if( $slide_speed == 0 ) $slide_speed = 1000;?>
	<script type="text/javascript">
    $slider = {
    context: false,
    tabs: false,
    timeout: 4500,      // time before next slide appears (in ms)
    slideSpeed: <?php echo $slide_speed;?>,   // time it takes to slide in each slide (in ms)
    tabSpeed: 300,      // time it takes to slide in each slide (in ms) when rotating through tabs
    fx: '<?php echo $slider_control;?>',   // slide control: fade, scrollLeft, right etc.
    
    init: function() {
        // set the context to help speed up selectors/improve performance
        this.context = $('#slider');
        
        // set tabs to current hard coded navigation items
        this.tabs = $('ul.slides-nav li', this.context);
        
        // remove hard coded navigation items from DOM 
        // because they aren't hooked up to jQuery cycle
        this.tabs.remove();
        
        // prepare slider and jQuery cycle tabs
        this.prepareSlideshow();
        
        $('#slider .slides li').css('display','block');
    },
    
    prepareSlideshow: function() {
        // initialise the jquery cycle plugin -
        // for information on the options set below go to: 
        // http://malsup.com/jquery/cycle/options.html
        $('div.slides > ul', $slider.context).cycle({
            fx: $slider.fx,
            timeout: $slider.timeout,
            speed: $slider.slideSpeed,
            fastOnEvent: $slider.tabSpeed,
            pager: $('ul.slides-nav', $slider.context),
            pagerAnchorBuilder: $slider.prepareTabs,
            before: $slider.activateTab,
            pauseOnPagerHover: true,
            pause: true
        });            
    },
    prepareTabs: function(i, slide) {
        // return markup from hardcoded tabs for use as jQuery cycle tabs
        // (attaches necessary jQuery cycle events to tabs)
        return $slider.tabs.eq(i);
    },
    activateTab: function(currentSlide, nextSlide) {
        // get the active tab
        var activeTab = $('a[href="#' + nextSlide.id + '"]', $slider.context);
        
        // if there is an active tab
		if(activeTab.length) {
            // remove active styling from all other tabs
            $slider.tabs.removeClass('on');
            // add active styling to active button
            activeTab.parent().addClass('on');
        }            
    }            
};

$(function() {
    // add a 'js' class to the body
    $('body').addClass('js');    
    // initialise the slider when the DOM is ready
    $slider.init();
	 jQuery("#compare_review_table").tablesorter();
});  
</script>   
  <!-- Google Plus One -->
  <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
 
<?php } add_action( 'wp_head', 'pro_header_codes' );

/*-----------------------------------------------------------------------------------*/
/* RSS Support for Custom Post Types
/*-----------------------------------------------------------------------------------*/

function myfeed_request($qv) {
	if (isset($qv['feed']))
		$qv['post_type'] = get_post_types( array( 'public' => true ) );
	return $qv;
}
add_filter('request', 'myfeed_request');

function prt_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}


/*
  Twitter 
*/



if (!class_exists('Twitter_Widget')) :

	class Twitter_Widget extends WP_Widget {


		function Twitter_Widget() {
									
			// Widget settings
			$widget_ops = array('classname' => 'twitter-widget', 'description' => 'Display your latest tweets.');

			// Create the widget
			$this->WP_Widget('twitter-widget', 'Proreview Twitter', $widget_ops);
		}
		
		
		function widget($args, $instance) {
			
			extract($args);
			
			global $interval;
			
			// User-selected settings
			$title = apply_filters('widget_title', $instance['title']);
			$username = $instance['username'];
			$posts = $instance['posts'];
			$interval = $instance['interval'];
			$clickable = $instance['clickable'];
			$hideerrors = $instance['hideerrors'];
			$encodespecial = $instance['encodespecial'];

			// Before widget (defined by themes)
			echo $before_widget;

			// Set internal Wordpress feed cache interval, by default it's 12 hours or so
			add_filter('wp_feed_cache_transient_lifetime', array(&$this, 'setInterval'));
			include_once(ABSPATH . WPINC . '/feed.php');

			// Get current upload directory
			$upload = wp_upload_dir();
			$cachefile = $upload['basedir'] . '/_twitter_' . $username . '.txt';

			// Title of widget (before and after defined by themes)
			if (!empty($title)) echo $before_title . $title . $after_title;

			// If cachefile doesn't exist or was updated more than $interval ago, create or update it, otherwise load from file
			if (!file_exists($cachefile) || (file_exists($cachefile) && (filemtime($cachefile) + $interval) < time())) :

				$feed = fetch_feed('http://api.twitter.com/1/statuses/user_timeline.rss?screen_name=' . $username);
				
				// This check prevents fatal errors ? which can't be turned off in PHP ? when feed updates fail
				if (method_exists($feed, 'get_items')) :

					$tweets = $feed->get_items(0, $posts);

					$result = '
						<ul class="twitter-list">';

					foreach ($tweets as $t) :
						$result .= '
							<li>';

						// Get message
						$text = $t->get_description();
						
						// Get date/time and convert to Unix timestamp
						$time = strtotime($t->get_date());

						// If status update is newer than 1 day, print time as "... ago" instead of date stamp
						if ((abs(time() - $time)) < 86400) :
							$time = human_time_diff($time) . ' ago';
						else :
							$time = date(($date), $time);
						endif;
						
						// HTML encode special characters like ampersands
						if ($encodespecial) :
							$text = htmlspecialchars($text);
						endif;

						// Make links and Twitter names clickable
						if ($clickable) :
							// Match URLs
							$text = preg_replace('`\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))`', '<a href="$0">$0</a>', $text);

							// Match @name
							$text = preg_replace('/(@)([a-zA-Z0-9\_]+)/', '@<a href="https://twitter.com/$2">$2</a>', $text);
							
							// Match #hashtag
							$text = preg_replace('/(#)([a-zA-Z0-9\_]+)/', '#<a href="https://twitter.com/search/?q=$2">$2</a>', $text);
						endif;

						// Display date/time
						if ($datedisplay) $result .= '
								<span class="twitter-date"><a href="'. $t->get_permalink() .'">' . $time . '</a></span>' . ($datebreak ? '<br />' : '');

// Display message without username prefix
						$prefixlen = strlen($username . ": ");
						$result .= '
								<span class="twitter-text">' . substr($text, $prefixlen, strlen($text) - $prefixlen) . ' &nbsp; <a class="twitter-date" href="'. $t->get_permalink() .'">' . $time . '</a></span>';
						$result .= '
							</li>';
					endforeach;
					
					$result .= '
						</ul>
						';
						$result .= '
								<span class="twitter-follow"><a class="twitter-follow-link" href="http://twitter.com/'. $username .'">Follow us &raquo; </a></span>';
						$result .= '';					
					// Save updated feed to cache file
					@file_put_contents($cachefile, $result);

					// Display everything
					echo $result;


				// If loading from Twitter fails, try loading from the file instead
				else :
					if (file_exists($cachefile)) :
						$result = @file_get_contents($cachefile);
					endif;

					if (!empty($result)) :
						echo $result;

					// If loading from the file failed too, display error
					elseif (!$hideerrors) :
						echo '<p>Error while loading Twitter feed.</p>';
					endif;
				endif;


			// If cache file exists or if it was updated not long ago, load from file straight away
			else :
				$result = @file_get_contents($cachefile);

				if (!empty($result)) :
					echo $result;
				elseif (!$hideerrors) :
					echo '<p>Error while loading Twitter feed.</p>';			
				endif;
			endif;


			// After widget (defined by themes)
			echo $after_widget;
		}
		
		
		// Callback helper for the cache interval filter
		function setInterval() {
			
			global $interval;
			
			return $interval;
		}

		
		function update($new_instance, $old_instance) {
			
			$instance = $old_instance;

			$instance['title'] = $new_instance['title'];
			$instance['username'] = $new_instance['username'];
			$instance['posts'] = $new_instance['posts'];
			$instance['interval'] = $new_instance['interval'];
			$instance['clickable'] = $new_instance['clickable'];
			$instance['hideerrors'] = $new_instance['hideerrors'];
			$instance['encodespecial'] = $new_instance['encodespecial'];
			
			// Delete the cache file when options were updated so the content gets refreshed on next page load
			$upload = wp_upload_dir();
			$cachefile = $upload['basedir'] . '/_twitter_' . $old_instance['username'] . '.txt';
			@unlink($cachefile);

			return $instance;
		}
		
		
		function form($instance) {

			// Set up some default widget settings
			$defaults = array('title' => 'Latest Tweets', 'username' => '', 'posts' => 5, 'interval' => 1800, 'date' => 'j F Y', 'datedisplay' => true, 'datebreak' => true, 'clickable' => true, 'hideerrors' => true, 'encodespecial' => false);
			$instance = wp_parse_args((array) $instance, $defaults);
?>
				
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
				<input class="widefat" type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>">
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('username'); ?>">Your Twitter username:</label>
				<input class="widefat" type="text" id="<?php echo $this->get_field_id('username'); ?>" name="<?php echo $this->get_field_name('username'); ?>" value="<?php echo $instance['username']; ?>">
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('posts'); ?>">Number of posts to display</label>
				<input class="widefat" type="text" id="<?php echo $this->get_field_id('posts'); ?>" name="<?php echo $this->get_field_name('posts'); ?>" value="<?php echo $instance['posts']; ?>">
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('interval'); ?>">Update interval (in seconds):</label>
				<input class="widefat" type="text" id="<?php echo $this->get_field_id('interval'); ?>" name="<?php echo $this->get_field_name('interval'); ?>" value="<?php echo $instance['interval']; ?>">
			</p>

		

				<input class="checkbox" type="checkbox" <?php if ($instance['clickable']) echo 'checked="checked" '; ?>id="<?php echo $this->get_field_id('clickable'); ?>" name="<?php echo $this->get_field_name('clickable'); ?>">
				<label for="<?php echo $this->get_field_id('clickable'); ?>">Clickable URLs, names &amp; hashtags</label>
				
				<br>

				<input class="checkbox" type="checkbox" <?php if ($instance['hideerrors']) echo 'checked="checked" '; ?>id="<?php echo $this->get_field_id('hideerrors'); ?>" name="<?php echo $this->get_field_name('hideerrors'); ?>">
				<label for="<?php echo $this->get_field_id('hideerrors'); ?>">Hide error message if update fails</label>

				<br>

				<input class="checkbox" type="checkbox" <?php if ($instance['encodespecial']) echo 'checked="checked" '; ?>id="<?php echo $this->get_field_id('encodespecial'); ?>" name="<?php echo $this->get_field_name('encodespecial'); ?>">
				<label for="<?php echo $this->get_field_id('encodespecial'); ?>">HTML-encode special characters</label>
			</p>
			
<?php
		}
	} 
endif;

// Register the plugin/widget
if (class_exists('Twitter_Widget')) :

	function loadTwitterWidget() {
		
		register_widget('Twitter_Widget');
	}

	add_action('widgets_init', 'loadTwitterWidget');

endif;

function prt_tag_cloud_filter($args = array()) {
   $args['smallest'] = 8;
   $args['largest'] = 8;
   $args['unit'] = 'pt';
   return $args;
}

add_filter('widget_tag_cloud_args', 'prt_tag_cloud_filter', 90);


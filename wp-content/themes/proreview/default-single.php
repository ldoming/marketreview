<?php get_header(); ?>        
    

<div class="main_content padd">
    <div id="primary">       

        
        <div class="standard_post_items single">
<?php   
 if (have_posts()) : while (have_posts()) : the_post();
?>     
       <div class="post_item">
                <h1><?php the_title(); ?></h1>
                <p class="post_meta"><?php _e("Posted by",'prt');?>: <?php the_author();?>  <?php _e("  on",'prt');?> <?php the_time('F j, Y');?>  <?php _e("Under",'prt');?>: <?php echo get_the_term_list( $post->ID, 'category', '', ', ', '' ); ?> | <g:plusone size="small"></g:plusone></p>
                <?php the_content(); ?>
				<?php link_pages('<p><strong>Pages:</strong> ', '</p>', 'number'); ?>	
				<p class="meta-tags"><?php the_tags(); ?></p>     				
            </div><!--end post_item-->
 
	
<?php endwhile; endif; ?>            
            
        	<div class="reviews_pagination">        
            			
            		<div class="page_navigation">				
        
            				 <div id="more_prev">                
            				 	<div class="previous_link"><?php next_post_link( '%link', __( __("Newer Post", 'prt') ) ); ?></div>
            				 	<div class="newer_links"><?php previous_post_link( '%link', __( __("Older Post", 'prt' ) ) ); ?></div>                
            				 </div><!--end more_prev -->		
            		
            		</div>    
            </div> <!--end reviews_pagination-->

        <!--end standard_post_items-->
<div id="comments_template">
<?php comments_template(); ?>
</div>
</div>
</div><!--end primary-->
        
<?php get_sidebar(); ?>

        </div><!--end main_content-->
        
<?php get_footer(); ?>
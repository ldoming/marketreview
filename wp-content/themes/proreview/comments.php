<?php
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');
if ( post_password_required() ) {
	echo 'This post is password protected. Enter the password to view comments.';
	return;
}
?>

<?php if ( have_comments() ) : ?>
    <h3 id="comments"><?php comments_number(__('No Comments',"prt"), __('One Comment',"prt"), __('% Comments',"prt") );?></h3>
     
    <ul class="commentlist">
		<?php wp_list_comments('type=comment&avatar_size=60&callback=mytheme_comment'); ?>
	</ul>
	
    
<?php else : // this is displayed if there are no comments so far ?>

	<?php if ($post->comment_status == 'open') : ?>
        <p><?php _e("There are no comments yet, add one below.",'prt');?></p>
    <?php else : ?>
        <p><?php _e("Comments are closed.",'prt');?></p>
    <?php endif; ?>

<?php endif; ?>



<?php if ('open' == $post->comment_status) : ?>





<?php include(TEMPLATEPATH . '/includes/functions/comment_form.php') ?>
<?php endif; ?>
<?php paginate_comments_links();  ?>
<div class="clear"></div>
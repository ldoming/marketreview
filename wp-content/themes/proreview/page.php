<?php get_header(); ?>        
    

<div class="main_content padd">
    <div id="primary">       

        
        <div class="standard_post_items single">
<?php   
 if (have_posts()) : while (have_posts()) : the_post();
?>     
       <div class="post_item">
                <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php the_content(); ?>
            </div><!--end post_item-->
<?php endwhile; endif; ?>            
            

        </div><!--end standard_post_items-->
        </div><!--end primary-->
        
<?php get_sidebar(); ?>

        </div><!--end main_content-->
        
<?php get_footer(); ?>
<?php


/*** Custom Post type Reviews ***/

add_action('init', 'reviews_register');

	function reviews_register() {
    	$args = array(
        	'label' => __('Reviews'),
        	'singular_label' => __('Review'),
        	'public' => true,
        	'show_ui' => true,
        	'capability_type' => 'post',
        	'hierarchical' => false,
        	'rewrite' => true,
			'menu_icon' => get_stylesheet_directory_uri() . '/images/reviews_icon.png',
        	'supports' => array('title','author' , 'editor', 'thumbnail','comments','excerpt' ),
        );
         
    	register_post_type( 'reviews' , $args );
		flush_rewrite_rules( false );
}

/*** Taxonomy for portfolio post type ***/

/***Cats like ***/

register_taxonomy(

"review-cats", 

array("reviews"), 

array(
"hierarchical" => true,
"label" => "Categories",
"singular_label" => "Category",
"rewrite" => true,
)

);

/***Tags Like ***/

register_taxonomy(

"review-tags", 

array("reviews"), 

array(
"hierarchical" => false,
"label" => "Review Tags",
"singular_label" => "Review Tag",
'show_tagcloud' => true,
"rewrite" => true
)

);
?>
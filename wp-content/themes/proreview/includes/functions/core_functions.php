<?php


// Add Background Support

add_custom_background();

if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
	add_theme_support( 'post-thumbnails' );
    add_theme_support('automatic-feed-links');
}




function mytheme_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class('comment-body'); ?> id="li-comment-<?php comment_ID() ?>">
     <div id="comment-<?php comment_ID(); ?>">
      <div class="comment-author vcard">
         <?php  echo get_avatar($comment,$size='60',$default= get_stylesheet_directory_uri() . '/images/default_avatar.png' ); ?>
    </div>
         <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
      
      <?php if ($comment->comment_approved == '0') : ?>
         <em><?php _e('Your comment is awaiting moderation.') ?></em>
         <br />
      <?php endif; ?>
 
      <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>
  <?php $rating=get_comment_meta( $comment->comment_ID, 'rating' ); if($rating[0]){  ?>
  <div class="review_rating stars_<?php echo $rating['0']; ?>"><?php echo $rating['0']; ?> stars</div>
     <?php } comment_text(); ?>
 
      <div class="reply">
         <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>
     </div>
<div class="clear"></div>
     </li>
<?php
        }

?>
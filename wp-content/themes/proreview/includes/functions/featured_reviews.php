<div style="width:965px; margin:0 auto;position:relative;">
<?php 

$featured_reviews = (get_option('pr_custom_featured_reviews')) ? get_option('pr_custom_featured_reviews') : array();

$loop = 1;
foreach ($featured_reviews as $featured_review) {
	switch ($loop) {
		case '1':
			$class = 'first';
			break;
		case '2':
			$class = 'second';
			break;
		case '3':
			$class = 'third';
			break;
		case '4':
			$class = 'fourth';
			break;
		case '5':
			$class = 'fifth';
			break;
		
		default:
			break;
	}
	global $wpdb;
	$review = $wpdb->get_results(
		"
			SELECT `post_title`
			FROM $wpdb->posts
			WHERE `ID` = $featured_review
				AND `post_status` = 'publish'
		"
	);
	$links = wp_get_attachment_image_src( get_post_thumbnail_id($featured_review),"full");
?>

	<div class="preload_image <?php echo $class?>" style="max-height:101px;width: <?php echo get_option('pr_thumb_width'); ?>px;">
	<?php if( $links[0] ) { ?>
			<a class="" href="<?php echo $review_image_link; ?>">
				<img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo wp_get_attachment_url( get_post_thumbnail_id($featured_review) ); ?>&w=<?php echo get_option('pr_thumb_width'); ?>&h=<?php echo get_option('pr_thumb_height'); ?>" alt="<?php echo $review[0]->post_title; ?>" />
			</a>
	<?php } else { ?>
		<a class="<?php echo $class; ?>" href="<?php echo $review_image_link; ?>">
    		<img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo get_template_directory_uri();?>/images/noimg-280.png&w=<?php echo get_option('pr_thumb_width'); ?>&h=<?php echo get_option('pr_thumb_height'); ?>" alt="single_review" />
   		</a>
	<?php } ?>
	</div>
	<?php

	$loop++;
}

?>
</div>

<style type="text/css">
	.first, .second, .third, .fourth {
		margin-right: 38px;
	}
</style>
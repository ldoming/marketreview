<?php

/**** Bypass AutoPost Formatting ***/

function my_formatter($content) {
	$new_content = '';
	$pattern_full = '{(\[raw\].*?\[/raw\])}is';
	$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
	$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

	foreach ($pieces as $piece) {
		if (preg_match($pattern_contents, $piece, $matches)) {
			$new_content .= $matches[1];
		} else {
			$new_content .= wptexturize(wpautop($piece));
		}
	}

	return $new_content;
}

remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'my_formatter', 99);

/********************************************/

add_shortcode('success','success_msg');

function success_msg($atts,$content=null){
    
return '
<div class="success_sc">'
   . $content .
'</div>
';
}

add_shortcode('warning','warning_msg');

function warning_msg($atts,$content=null){
    
return '
<div class="warning_sc">'
   . $content .
'</div>
';
}


add_shortcode('message','message_msg');

function message_msg($atts,$content=null){
    
return '
<div class="message_sc">'
   . $content .
'</div>
';
}

add_shortcode('checkout','sc_checkout');

function sc_checkout($atts,$content=null){
     extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="check_out"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('downloadblue','sc_downloadblue');

function sc_downloadblue($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="download_blue"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('downloadred','sc_downloadred');

function sc_downloadred($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="download_red"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('visitsite','sc_visitsite');

function sc_visitsite($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="visit_site"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('tellmore','sc_tellmore');

function sc_tellmore($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="tell_more"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('biggreen','sc_biggreen');

function sc_biggreen($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="big_green"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('bigred','sc_bigred');

function sc_bigred($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="big_red"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('bigblue','sc_bigblue');

function sc_bigblue($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="big_blue"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('bigyellow','sc_bigyellow');

function sc_bigyellow($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="big_yellow"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('azcart','sc_azcart');

function sc_azcart($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="az_cart"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('azcheck','sc_azcheck');

function sc_azcheck($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="az_check"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('azprod','sc_azprod');

function sc_azprod($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="az_prod"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('azvisit','sc_azvisit');

function sc_azvisit($atts,$content=null){
      extract(shortcode_atts(array(
			'url' => '#'
    ), $atts));
    
return '

<a href="' .$url. '" class="az_visit"><span class="button_inner">' . $content . '</span></a>

';
}

add_shortcode('banner','sc_banner');

function sc_banner($atts,$content=null){
    
return '
<div class="uni_banner">
<a href="' . $atts['link'] . '"><img src="' . $content . '" alt="banner" /></a>
</div>
';
}

add_shortcode('checklist','check_list');

function check_list($atts,$content=null){
        

return '
<div class="check_list">'
   . $content .
'</div>
';
}

add_shortcode('errorlist','cross_list');

function cross_list($atts,$content=null){

    
return '
<div class="cross_list">'
   . $content .
'</div>
';
}

add_shortcode('playlist','play_list');

function play_list($atts,$content=null){
    
if($atts['last']){
    $last=' last';
}
else{$last='';}
        
return '
<div class="play_list' . $last .'">'
   . $content .
'</div>
';
}

add_shortcode('starlist','star_list');

function star_list($atts,$content=null){
      
if($atts['last']){
    $last=' last';
}
else{$last='';}
      
return '
<div class="star_list' . $last .'">'
   . $content .
'</div>
';
}

function embed_yt($atts, $content = null) {
extract(shortcode_atts(array(
'video'  => '',
'width'  => '',
'height' => ''
), $atts));

return '<div style="text-align: center; margin: auto"><object type="application/x-shockwave-flash" style="width:'.$width.'px; height:'.$height.'px;" data="http://www.youtube.com/v/'.$video.'&amp;hl=en_US&amp;fs=1&amp;"><param name="movie" value="http://www.youtube.com/v/'.$video.'&amp;hl=en_US&amp;fs=1&amp;" /></object></div>';
}
add_shortcode('youtube', 'embed_yt');


//Vimeo video take a tour shortcode

function fn_vimeo($atts, $content = null) {
   extract(shortcode_atts(array(
      "width" => '',
      "height" => '',
      "src" => ''
   ), $atts));
   return '<div style="text-align: center; margin: auto"><iframe width="'.$width.'" height="'.$height.'" frameborder="0" title="0" potrait="0" byline="0" src="http://player.vimeo.com/video/'.$src.'"></iframe></div>';
}
add_shortcode("vimeo", "fn_vimeo");

//Dropcap shortcode

function dropcap1($atts, $content = null) {
	return '
<div class="dropcap-1">'.$content.'</div>';
}
add_shortcode('dropcap-1', 'dropcap1');

function dropcap2($atts, $content = null) {
	return '
<div class="dropcap-2">'.$content.'</div>';
}
add_shortcode('dropcap-2', 'dropcap2');

function dropcap3($atts, $content = null) {
	return '
<div class="dropcap-3">'.$content.'</div>';
}
add_shortcode('dropcap-3', 'dropcap3');

function dropcap4($atts, $content = null) {
	return '
<div class="dropcap-4">'.$content.'</div>';
}
add_shortcode('dropcap-4', 'dropcap4');

function dropcap5($atts, $content = null) {
	return '
<div class="dropcap-5">'.$content.'</div>';
}
add_shortcode('dropcap-5', 'dropcap5');


function dropcapsilver($atts, $content = null) {
	return '
<div class="dropcap-silver">'.$content.'</div>';
}
add_shortcode('dropcap-silver', 'dropcapsilver');

function dropcapblack($atts, $content = null) {
	return '
<div class="dropcap-black">'.$content.'</div>';
}
add_shortcode('dropcap-black', 'dropcapblack');



//higlight shortcode

function yellow_highlight($atts, $content = null) {
extract(shortcode_atts(array(
    'color' => 'yellow',
    'font' => '#464646'
  ), $atts));
  return "<FONT style=\"padding: 2px 4px;margin:2px 3px;BACKGROUND-COLOR: $color; color: $font\">$content</font>";
}
add_shortcode('highlight-yellow','yellow_highlight');

function grey_highlight($atts, $content = null) {
extract(shortcode_atts(array(
    'color' => '#dcdcdc',
    'font' => '#464646'
  ), $atts));
  return "<FONT style=\"padding: 2px 4px;margin:2px 3px;BACKGROUND-COLOR: $color; color: $font\">$content</font>";
}
add_shortcode('highlight-grey','grey_highlight');

function blue_highlight($atts, $content = null) {
extract(shortcode_atts(array(
    'color' => '#c0e4f5',
    'font' => '#464646'
  ), $atts));
  return "<FONT style=\"padding: 2px 4px;margin:2px 3px;BACKGROUND-COLOR: $color; color: $font\">$content</font>";
}
add_shortcode('highlight-blue','blue_highlight');

/*
add_shortcode('half','one_half');

function one_half($atts,$content=null){
if($atts['last']){
    $last=' last';
}
else{$last='';}
return '
<div class="x1_2' . $last .'">'
   . $content .
'</div>
';
}

add_shortcode('fourth','one_fourth');

function one_fourth($atts,$content=null){
    
if($atts['last']){
    $last=' last';
}
else{$last='';}

return '
<div class="x1_4' . $last .'">'
   . $content .
'</div>
';
}

add_shortcode('third','one_third');

function one_third($atts,$content=null){
    
if($atts['last']){
    $last=' last';
}
else{$last='';}

return '
<div class="x1_3' . $last .'">'
   . $content .
'</div>
';
}

add_shortcode('twothird','two_third');

function two_third($atts,$content=null){
    
if($atts['last']){
    $last=' last';
}
else{$last='';}
    
return '
<div class="x2_3' . $last .'">'
   . $content .
'</div>
';
}

add_shortcode('threefourth','three_fourth');

function three_fourth($atts,$content=null){
    
if($atts['last']){
    $last=' last';
}
else{$last='';}
    
return '
<div class="x3_4' . $last .'">'
   . $content .
'</div>
';
}


add_shortcode('collapsable','collapsable');

function collapsable($atts,$content=null){
    
return '

<div class="exp_coll">
                    <p>' . $atts['title'] . '<span class="exp_icon"></span></p>
                    
                    <div class="exp_coll_body ' . $atts['status'] .'" style="display: block;">' .$content . '
                    </div><!--end exp_coll_body -->
                </div>';
}

add_shortcode( 'tabgroup', 'etdc_tab_group' );






function etdc_tab_group( $atts, $content ){
$GLOBALS['tab_count'] = 0;

do_shortcode( $content );

if( is_array( $GLOBALS['tabs'] ) ){
foreach( $GLOBALS['tabs'] as $tab ){
$tabs[] = '<li class="tab_m"><a href="#">'.$tab['title'].'</a></li>';
$panes[] = '<div class="pane">'.$tab['content'].'</div>';
}
$return = "\n".'<div class="arc_tabs">
     <!-- the tabs --><ul class="tabs">' . implode( "\n", $tabs ). '</ul>' . "\n" . '<!-- tab "panes" -->
    
    <div class="panes">' . implode( "\n", $panes ).'</div></div><!--arc_tabs-->'."\n";
}
return $return;
}



add_shortcode( 'tab', 'etdc_tab' );


function etdc_tab( $atts, $content ){
extract(shortcode_atts(array(
'title' => 'Tab %d'
), $atts));

$x = $GLOBALS['tab_count'];
$GLOBALS['tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['tab_count'] ), 'content' =>  $content );

$GLOBALS['tab_count']++;
}




/*
add_shortcode('img_left','img_left');

function img_left($atts){
    $atts=shortcode_atts(
    array(
        'caption' =>'',
        'src' =>'',  
        'height'=> '150',
        'width' => '150'  
    ),$atts);
$file_dir=get_bloginfo('template_directory');
$img_left_src = $file_dir . '/timthumb.php?src=' . $atts['src'] . '&h=' . $atts['height']. '&w=' . $atts['width'] .'&zc=1" width="' . $atts['width'] . '" height="' . $atts['height'] ;

return '
<div class="img_left">
    <a href="' . $atts['src'] . '" /><img src="' . $img_left_src . '" /></a>
    <p class="img_cap">' .$atts['caption'] . '</p>
</div>
';
}

add_shortcode('img_right','img_right');

function img_right($atts){
    $atts=shortcode_atts(
    array(
        'caption' =>'',
        'src' =>'',  
        'height'=> '150',
        'width' => '150'  
    ),$atts);
$file_dir=get_bloginfo('template_directory');
$img_right_src = $file_dir . '/timthumb.php?src=' . $atts['src'] . '&h=' . $atts['height']. '&w=' . $atts['width'] .'&zc=1" width="' . $atts['width'] . '" height="' . $atts['height'] ;

return '
<div class="img_right">
   <a href="' . $atts['src'] . '"> <img src="' . $img_right_src . '" /></a>
    <p class="img_cap">' .$atts['caption'] . '</p>
</div>
';
}
*/
<?php

// include the class in your theme or plugin

include_once 'WPAlchemy/MetaBox.php';
 
// include css to help style our custom meta boxes
if (is_admin()) { wp_enqueue_style('custom_meta_css', get_bloginfo('stylesheet_directory') . '/includes/functions/custom/meta.css'); }
 
$custom_metabox = new WPAlchemy_MetaBox(array
(
	'id' => '_reviews_meta',
	'title' => 'Review Details',
	'template' => STYLESHEETPATH . '/includes/functions/custom/simple_meta.php',
    'types' => array('reviews'),
    'save_filter' => 'update_review_ratings'
));
$custom_metabox2 = new WPAlchemy_MetaBox(array
(
	'id' => '_conclusion_meta',
	'title' => 'Conclusion Box',
	'template' => STYLESHEETPATH . '/includes/functions/custom/conclusion.php',
    'types' => array('reviews')
));

function update_review_ratings($meta,$post_id){
    
    update_post_meta($post_id, 'pr_rating', $meta['r_rating']);
    
    return $meta;
}
?>
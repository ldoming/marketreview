<?php

add_action( 'init', 'register_my_menus' );

function register_my_menus() {
	register_nav_menus(
		array(
            'top-menu' => __( 'Top Menu' ),
			'footer-menu' => __( 'Footer Menu' ),
            'cat-menu' => __( 'Categories Menu' )
		)
	);
}      
add_action( 'init', 'create_my_menus' );      
function create_my_menus(){
    wp_create_nav_menu( 'Top Menu', array( 'slug' => 'top-menu' ) );
    wp_create_nav_menu( 'Footer Menu', array( 'slug' => 'footer-menu' ) );
    wp_create_nav_menu( 'Categories Menu', array( 'slug' => 'cat-menu' ) );
}

?>
<?php

$wp_include = "../wp-load.php";
$i = 0;
while (!file_exists($wp_include) && $i++ < 10) {
  $wp_include = "../$wp_include";
}

// let's load WordPress
require($wp_include);

if ( !is_user_logged_in() || !current_user_can('edit_posts') ) 
	wp_die(__("You are not allowed to be here"));
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>PROREVIEW MODULES AND ELEMENTS</title>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php echo get_option('blog_charset'); ?>" />
<script language="javascript" type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/utils/mctabs.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/utils/form_utils.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri() ?>/includes/functions/tinymceModules/tinymce.js"></script>
<style type="text/css">
fieldset { margin:16px 0; padding:10px; }
legend, label, select, input[type=text] { font-size:11px; }
select, input[type=text] { line-height:24px; height:24px; float:left; width:300px }
</style>
</head>
<body onLoad="tinyMCEPopup.executeOnLoad('init();');">
<form name="pro_modules" action="#">
			<!-- style_panel -->
			<fieldset>
						<legend>Modules and elements</legend>
						<select id="module_shortcode" name="module_shortcode">
									<optgroup label="Lists">
									<option value="checklist">Check List</option>
									<option value="errorlist">Cross List</option>
									<option value="playlist">Arrows List</option>
									<option value="starlist">Star List</option>
									</optgroup>
									
									<optgroup label="Call To Action Buttons">
									<option value="checkout">Check it out - Button</option>
                                    <option value="tellmore">Tell me more - Button</option>
                                    <option value="downloadblue">Download Blue - Button</option>
                                    <option value="downloadred">Download Red - Button</option>
                                    <option value="visitsite">Visit site now - Button</option>
                                    <option value="biggreen">Big Green - Button</option>
                                    <option value="bigred">Big Red - Button</option>
                                    <option value="bigblue">Big Blue - Button</option>
                                    <option value="bigyellow">Big Yellow - Button</option>
									</optgroup>
                                     <optgroup label="Amazon Style Buttons">
									<option value="azcart">Add To Cart - Button</option>
                                    <option value="azcheck">Check it out - Button</option>
                                    <option value="azprod">View Product - Button</option>
                                    <option value="azvisit">Visit Website - Button</option>
									</optgroup>
									<optgroup label="Message Boxes">
									<option value="success">Success Box</option>
									<option value="warning">Warning Box</option>
									<option value="message">Message Box</option>
									</optgroup>
									<optgroup label="Text Higlight">
									<option value="highlight-blue">Blue</option>
									<option value="highlight-yellow">Yellow</option>
									<option value="highlight-grey">Grey</option>
									
									</optgroup>										
									<optgroup label="Dropcap">
									<option value="dropcap-1">Dropcap Italic Latin</option>
									<option value="dropcap-2">Dropcap Latin </option>
									<option value="dropcap-3">Dropcap Circle</option>
									<option value="dropcap-4">Dropcap Square</option>
									<option value="dropcap-5">Dropcap Rounded Corner</option>
									<option value="dropcap-silver">Dropcap Silver</option>
									<option value="dropcap-black">Dropcap Black</option>
									</optgroup>														
									<optgroup label="Video">
									<option value="youtube">Youtube</option>
									<option value="vimeo">Vimeo</option>
									</optgroup>	
												
						</select>
			</fieldset>
			<input type="button" id="cancel" name="cancel" value="Cancel" onClick="tinyMCEPopup.close();"  style="float:left; padding:10px; width:auto; height:auto;"/>
			<input type="submit" id="insert" name="insert" value="Insert shortcode" onClick="insertShortcode();" style="float:right; padding:10px; width:auto; height:auto;"/>
</form>
</body>
</html>

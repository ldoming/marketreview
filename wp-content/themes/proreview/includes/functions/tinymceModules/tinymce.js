function insertShortcode() {
	
	var shortcodeText;
	var shortcodeId = document.getElementById('module_shortcode').value;
	
	
	if (shortcodeId != 0 && shortcodeId == 'checklist' ){
		shortcodeText = '[checklist]Add list elements here[/checklist]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'errorlist' ){
		shortcodeText = '[errorlist]Add list elements here[/errorlist]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'playlist' ){
		shortcodeText = '[playlist]Add list elements here[/playlist]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'starlist' ){
		shortcodeText = '[starlist]Add list elements here[/starlist]';	
		}
	
	// Call to action buttons
		
	if (shortcodeId != 0 && shortcodeId == 'checkout' ){
		shortcodeText = '[checkout url="#"]check it out[/checkout]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'tellmore' ){
		shortcodeText = '[tellmore url="#"]Tell me more[/tellmore]';	
		}
	
	if (shortcodeId != 0 && shortcodeId == 'downloadblue' ){
		shortcodeText = '[downloadblue url="#"]Download now[/downloadblue]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'downloadred' ){
		shortcodeText = '[downloadred url="#"]Download now[/downloadred]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'visitsite' ){
		shortcodeText = '[visitsite url="#"]Visit site now[/visitsite]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'biggreen' ){
		shortcodeText = '[biggreen url="#"]Check it now[/biggreen]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'bigred' ){
		shortcodeText = '[bigred url="#"]Check it now[/bigred]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'bigblue' ){
		shortcodeText = '[bigblue url="#"]Check it now[/bigblue]';	
		}
		
    if (shortcodeId != 0 && shortcodeId == 'bigyellow' ){
		shortcodeText = '[bigyellow url="#"]Check it now[/bigyellow]';	
		}
		
    if (shortcodeId != 0 && shortcodeId == 'azcart' ){
		shortcodeText = '[azcart url="#"]Check it now[/azcart]';	
		}
	
	if (shortcodeId != 0 && shortcodeId == 'azcheck' ){
		shortcodeText = '[azcheck url="#"]Check it now[/azcheck]';	
		}
		
	if (shortcodeId != 0 && shortcodeId == 'azprod' ){
		shortcodeText = '[azprod url="#"]Check it now[/azprod]';	
		}
	
	if (shortcodeId != 0 && shortcodeId == 'azvisit' ){
		shortcodeText = '[azvisit url="#"]Check it now[/azvisit]';	
		}
		
	// Message boxes
		
	if (shortcodeId != 0 && shortcodeId == 'success' ){
		shortcodeText = '[success]Add here your success message...[/success]';	
		}
	
	if (shortcodeId != 0 && shortcodeId == 'warning' ){
		shortcodeText = '[warning]Add here your warning message...[/warning]';	
		}
	
	if (shortcodeId != 0 && shortcodeId == 'message' ){
		shortcodeText = '[message]Add here your message...[/message]';	
		}
	
	if (shortcodeId != 0 && shortcodeId == 'youtube' ){
		shortcodeText = '[youtube video="Video ID" width=550 height=350  /]';	
		}		
	if (shortcodeId != 0 && shortcodeId == 'vimeo' ){
		shortcodeText = '[vimeo src="Video ID" width=550 height=350  /]';	
		}	
	if (shortcodeId != 0 && shortcodeId == 'dropcap-1' ){
		shortcodeText = '[dropcap-1]Your First Letter[/dropcap-1]';	
		}	
	if (shortcodeId != 0 && shortcodeId == 'dropcap-2' ){
		shortcodeText = '[dropcap-2]Your First Letter[/dropcap-2]';	
		}	
	if (shortcodeId != 0 && shortcodeId == 'dropcap-3' ){
		shortcodeText = '[dropcap-3]Your First Letter[/dropcap-3]';	
		}	
	if (shortcodeId != 0 && shortcodeId == 'dropcap-4' ){
		shortcodeText = '[dropcap-4]Your First Letter[/dropcap-4]';	
		}	
	if (shortcodeId != 0 && shortcodeId == 'dropcap-5' ){
		shortcodeText = '[dropcap-5]Your First Letter[/dropcap-5]';	
		}			
	if (shortcodeId != 0 && shortcodeId == 'dropcap-black' ){
		shortcodeText = '[dropcap-black]Your First Letter[/dropcap-black]';	
		}			
	if (shortcodeId != 0 && shortcodeId == 'dropcap-silver' ){
		shortcodeText = '[dropcap-silver]Your First Letter[/dropcap-silver]';	
		}		
	if (shortcodeId != 0 && shortcodeId == 'highlight-yellow' ){
		shortcodeText = '[highlight-yellow][/highlight-yellow]';	
		}			
	if (shortcodeId != 0 && shortcodeId == 'highlight-grey' ){
		shortcodeText = '[highlight-grey][/highlight-grey]';	
		}		
	if (shortcodeId != 0 && shortcodeId == 'highlight-blue' ){
		shortcodeText = '[highlight-blue][/highlight-blue]';	
		}				
	if ( shortcodeId == 0 ){
			tinyMCEPopup.close();
		}	
	
	if(window.tinyMCE) {
		//TODO: For QTranslate we should use here 'qtrans_textarea_content' instead 'content'
		window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false, shortcodeText);
		//Peforms a clean up of the current editor HTML. 
		//tinyMCEPopup.editor.execCommand('mceCleanup');
		//Repaints the editor. Sometimes the browser has graphic glitches. 
		tinyMCEPopup.editor.execCommand('mceRepaint');
		tinyMCEPopup.close();
	}
	return;
}

<div class="my_meta_control">
 
 
	<label>Review Top Title</label>
 
	<p>
		<?php $mb->the_field('r_title'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
		<span>Enter a custom title that appears at the top of the Review</span>
	</p>
 
	<label>Review Brief Description</label>
 
	<p>
		<?php $mb->the_field('r_description'); ?>
		<textarea name="<?php $mb->the_name(); ?>" rows="3"><?php $mb->the_value(); ?></textarea>
		<span>Enter in a description</span>
	</p>
    
    <label>Your Affiliate Link</label>
 
	<p>
		<?php $mb->the_field('r_aff_link'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
		<span>Enter in a link</span>
	</p>
    
    
    <label>Rating</label><br/>
	<p>
	<?php $mb->the_field('r_rating'); ?>
	<select name="<?php $mb->the_name(); ?>">
		<option value="5"<?php $mb->the_select_state('5'); ?>>5 stars</option>
        <option value="4_5"<?php $mb->the_select_state('4_5'); ?>>4.5 stars</option>
		<option value="4"<?php $mb->the_select_state('4'); ?>>4 stars</option>
        <option value="3_5"<?php $mb->the_select_state('3_5'); ?>>3.5 stars</option>
		<option value="3"<?php $mb->the_select_state('3'); ?>>3 stars</option>
        <option value="2_5"<?php $mb->the_select_state('2_5'); ?>>2.5 stars</option>
        <option value="2"<?php $mb->the_select_state('2'); ?>>2 stars</option>
        <option value="1_5"<?php $mb->the_select_state('1_5'); ?>>1.5 stars</option>
        <option value="1"<?php $mb->the_select_state('1'); ?>>1 star</option>
        <option value="0_5"<?php $mb->the_select_state('0_5'); ?>>0.5 stars</option>
        <option value="0"<?php $mb->the_select_state('0'); ?>>0 stars</option>
	</select>
    </p>
    <label>Price</label>
    <p>
		<?php $mb->the_field('r_price'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
		<span>Enter in Price - Leave blank to disable "Price" field</span>
	</p>
    
    <label>Ribbon</label><br/>
	<p>
	<?php $mb->the_field('r_ribbon'); ?>
	<select name="<?php $mb->the_name(); ?>">
		<option value=""<?php $mb->the_select_state('no'); ?>>No Ribbon</option>
		<option value="recommended"<?php $mb->the_select_state('recommended'); ?>>Recommended</option>
        <option value="popular"<?php $mb->the_select_state('popular'); ?>>Popular</option>
        <option value="bestoption"<?php $mb->the_select_state('bestoption'); ?>>Best Option</option>
		<option value="toprated"<?php $mb->the_select_state('toprated'); ?>>Top Rated</option>
        <option value="offer"<?php $mb->the_select_state('specialoffer'); ?>>Special Offer</option>
        <option value="bigopportunity"<?php $mb->the_select_state('bigopportunity'); ?>>Big Opportunity</option>
        <option value="average"<?php $mb->the_select_state('average'); ?>>Average</option>
		<option value="notrecommended"<?php $mb->the_select_state('notrecommended'); ?>>Not Recommended</option>
	</select>
    </p>
    
    <label>Disable OverviewBox on Single Review Page</label>
 
	<p> 
        <?php $mb->the_field('r_overview'); ?>
        &nbsp;&nbsp;<input type="checkbox" name="<?php $mb->the_name(); ?>" value="overview"<?php $mb->the_checkbox_state('overview'); ?>/>
	   	<span>&nbsp;&nbsp;Check to Disable "Overview Box"</span>	
	
	</p>
</div>
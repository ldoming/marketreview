<div class="my_meta_control">
 
<?php while($mb->have_fields_and_multi('c_group')): ?>
	<?php $mb->the_group_open(); ?>
	<label>Concept Name</label>
 
	<p>
		<?php $mb->the_field('c_title'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
		<span>Enter in a Name</span>
	</p>    
    
    <label>Rating</label><br/>
	<p>
	<?php $mb->the_field('c_rating'); ?>
	<select name="<?php $mb->the_name(); ?>">
        <option value="">Select...</option>
		<option value="5"<?php $mb->the_select_state('5'); ?>>5 stars</option>
		<option value="4"<?php $mb->the_select_state('4'); ?>>4 stars</option>
		<option value="3"<?php $mb->the_select_state('3'); ?>>3 stars</option>
        <option value="2"<?php $mb->the_select_state('2'); ?>>2 stars</option>
        <option value="1"<?php $mb->the_select_state('1'); ?>>1 star</option>
	</select>
    </p>
    <p><a href="#" class="dodelete button">delete</a></p>
<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>
    <p><a href="#" class="docopy-c_group button">Add</a></p>

    
    
</div>
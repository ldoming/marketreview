<?php
function create_posts() {
	
	if(!get_page_by_title('All Reviews'))
	{
	// Create All Reviews page
	 $all_review_page = array(
		 'post_title' => 'All Reviews',
		 'post_type' => 'page',
		 'post_content' => '',
		 'post_status' => 'publish',
		 'post_author' => 1
	  );
	
	 // Insert the post into the database
	  $newPost = wp_insert_post( $all_review_page );
	  update_post_meta($newPost, '_wp_page_template',  "reviews.php");
	  update_option("pr_reviews_ID",$newPost) ;
	}
	if(!get_page_by_title('Blog'))
	{
	 // Create All blog page
	 $blog_page = array(
		 'post_title' => 'Blog',
		 'post_type' => 'page',
		 'post_content' => '',
		 'post_status' => 'publish',
		 'post_author' => 1
	  );
	
	// Insert the post into the database
	  $newPost = wp_insert_post( $blog_page );
	  update_post_meta($newPost, '_wp_page_template',  "blog.php");
	  update_option("pr_blog_ID",$newPost) ;
	}
}
add_action("after_switch_theme", "create_posts");

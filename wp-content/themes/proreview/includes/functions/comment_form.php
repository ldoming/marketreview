<?php
global $user_identity, $id;
?>
<div id="respond">
<h3><?php _e("Leave a reply",'prt');?></h3>
<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
<p class="comment-notes"><?php _e("Your email address will not be published. Required fields are marked",'prt');?> <span class="required">*</span></p>
<div class="column_left">

<p>
<label for="author"><?php _e("Name",'prt');?></label><span class="required">*</span>
<input type="text" name="author" id="author" value="" size="22" tabindex="1" /> 
</p> 

<p><label for="email"><?php _e("Email",'prt');?></label><span class="required">*</span>
<input type="text" name="email" id="email" value="" size="22" tabindex="2"  /> 
</p> 

<p><label for="url"><?php _e("Website",'prt');?></label>
<input type="text" name="url" id="url" value="" size="22" tabindex="2"  /> 
</p> 

</div><!--end column left-->

<div class="column_right">

<p><label for="comment"><?php _e("Your Comment/Review",'prt');?></label>
<textarea name="comment" id="comment"  onFocus="clearText(this)" onBlur="clearText(this)" ></textarea></p> 

<p  class="comment_ratings"><label for="rating"><?php _e("Your Rating",'prt');?> :</label>
<input id="rating" name="rating" type="hidden" value="" />

<input name="star1" type="radio" class="star" value="1" />
<input name="star1" type="radio" class="star" value="2" />
<input name="star1" type="radio" class="star" value="3" />
<input name="star1" type="radio" class="star" value="4" />
<input name="star1" type="radio" class="star" value="5" />
</p>
<p><input name="submit" type="submit" id="submit" tabindex="5" value="Submit" />
</div><!--end column_right-->
 
<?php comment_id_fields(); ?>

<?php do_action('comment_form', $post->ID); ?>
</form>
</div>
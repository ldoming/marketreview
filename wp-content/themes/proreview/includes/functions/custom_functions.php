<?php

/************************************************************
********************Custom Theme Title Function***********
************************************************************/

function theme_title($limit) {
    //rm is read more
 global $post;

	$title = get_the_title();
	$title = $title . " ";
    
	if( strlen($title) > $limit )
		$ellipsis = true;

	$title = substr($title,0,$limit);

	$title = substr($title,0,strrpos($title,' ')); 

	if( $ellipsis == true )
		$title = $title . "...";

  return $title;
}

/************************************************************
********************Custom Theme Excerpt Function***********
************************************************************/

function theme_excerpt($limit,$rm) {
    //rm is read more
 global $post;

	$excerpt = get_the_content();
	
	//preserve the formatiing
	$excerpt = apply_filters('the_content', $excerpt);
	$excerpt = str_replace(']]>', ']]>', $excerpt);
	
	$excerpt = $excerpt . " ";
	$excerpt = strip_tags($excerpt); 
    
	if( strlen($excerpt) > $limit )
		$ellipsis = true;

	$excerpt = substr($excerpt,0,$limit);

	$excerpt = substr($excerpt,0,strrpos($excerpt,' ')); 

	if( $ellipsis == true )
		$excerpt = $excerpt . "...";


  if($rm){
  $excerpt='<p class="theme_excerpt">' .$excerpt . '</p><a href="'. get_permalink($post->ID) . '" class="more" ><span>' . $rm . '</span></a>';
  }
  else{
   $excerpt='<p class="theme_excerpt">' .$excerpt . '</p>';
  }
  return $excerpt;
}

/********************************************************************************
******************** Custom Funnction to print thumbnail using timthumb *********
********************************************************************************/

function print_thumbnail_src($width,$height){
    if ( has_post_thumbnail() ){
$attach_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),full);
}
#$timb_final_src=get_bloginfo('template_directory') . '/timthumb.php?src=' . $attach_src[0] . '&amp;h=' . $height . '&amp;w=' . $width . '&amp;zc=1';
$timb_final_src=get_bloginfo('template_directory') . '/timthumb.php?src=' . str_replace(get_bloginfo('url'),"",$attach_src[0]) . '&amp;h=' . $height . '&amp;w=' . $width . '&amp;zc=1';
echo $timb_final_src;
}


/***************************************************************************************
**********Custom Function to Add extra field to Comments (rating in this case)**********
****************************************************************************************/


function pr_rating_fields($fields)
{
  global $post;
  if( is_singular( 'reviews' ) ) // this is our CPT
  {
    
    $fields['rating'] = '<p class="comment-form-rating">' . '<label for="rating">' . __( 'Rating' ) . '</label> ' .
      '<input id="rating" name="rating" type="hidden" value="' . esc_attr( $commenter['rating'] ) . '" >
        <input name="star1" type="radio" class="star" value="1" />
<input name="star1" type="radio" class="star" value="2" />
<input name="star1" type="radio" class="star" value="3" />
<input name="star1" type="radio" class="star" value="4" />
<input name="star1" type="radio" class="star" value="5" /></p>';

    $fields = array( $fields['author'], $fields['email'], $fields['url'],$fields['rating'] );
  }
  return $fields;
}

add_filter( 'comment_form_default_fields', 'pr_rating_fields' );

function pr_add_author_rating($comment_id)
{
  add_comment_meta( $comment_id, 'rating', $_POST['rating'], true );
}

add_action( 'comment_post', 'pr_add_author_rating', 1 );


?>
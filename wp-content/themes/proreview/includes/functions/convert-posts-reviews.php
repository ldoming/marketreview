<?php
add_action('admin_menu', 'register_my_custom_submenu_page');

function register_my_custom_submenu_page() {
	add_submenu_page( 'edit.php?post_type=reviews', 'Clone Posts', 'Clone Posts', 'manage_options', 'clone-posts', 'my_custom_submenu_page_callback' ); 
}

function my_custom_submenu_page_callback() {
	if(isset($_POST['doclone']) && $_POST['doclone'] != ''){
	echo "<h2>Creating duplicates of all posts to Proreview posts.......</h2>";
	global $wpdb;
	$query = "SELECT * FROM $wpdb->posts WHERE post_type='post' and post_status != 'auto-draft' ORDER BY ID DESC";
	$result = $wpdb->get_results($query);
	if(isset($result) && sizeof($result) > 0){
		foreach($result as $post){
			$status = $post->post_status;
			$new_id = duplicate_post_create_duplicate($post, $status);
			duplicate_post_copy_post_taxonomies($new_id, $post);
			duplicate_post_copy_post_meta_info($new_id, $post);
			duplicate_post_copy_children($new_id, $post);
			if(isset($_POST['delposts']) && $_POST['delposts'] == 'yes'){
				wp_delete_post( $post->ID, TRUE );
			}
		}
	}
	echo '<h2>.</h2><br />';
	echo '<h2>Done...</h2>';
	} else {
		?>
		<form name="clone" method="post" action="">
		<?php
		echo "<h2>Clone your Blog Posts into Reviews</h2><br>
		<input type='checkbox' name='delposts' value='yes' />&nbsp; Delete posts after copying to Reviews:<br><br>
		Check this box if you want to automatically delete the Posts after they have been copied to Reviews. Leave this option unchecked if you want to delete your posts manually.<br><br>
		This function lets your copy your posts into reviews. You can then customize your posts with Review sepcific details such as Ratings, Affiliate Link and Price. Onec the reviews have been copied they will be removed from the default Posts section and appear in Reviews section.<br>";
		?>
			<input type="hidden" name="doclone" value="1" />
			<input type="submit" value="Convert Posts to Reviews" />
		</form>
		<?php
	}
	//$new_id = duplicate_post_create_duplicate($post, $status);
}

function duplicate_post_create_duplicate($post, $status = '', $parent_id = '') {

// We don't want to clone revisions
if ($post->post_type == 'revision') return;

$new_post_author = duplicate_post_get_current_user();

$new_post = array(
'menu_order'     => $post->menu_order,
'comment_status' => $post->comment_status,
'ping_status'    => $post->ping_status,
'post_author'    => $new_post_author->ID,
'post_content'   => $post->post_content,
'post_excerpt'   => (get_option('duplicate_post_copyexcerpt') == '1') ? $post->post_excerpt : "",
'post_mime_type' => $post->post_mime_type,
'post_parent'    => $new_post_parent = empty($parent_id)? $post->post_parent : $parent_id,
'post_password'  => $post->post_password,
'post_status'    => $new_post_status = (empty($status))? $post->post_status: $status,
'post_title'     => $prefix.$post->post_title.$suffix,
'post_type'      => 'reviews',
);

if(get_option('duplicate_post_copydate') == 1){
$new_post['post_date'] = $new_post_date = $post->post_date ;
$new_post['post_date_gmt'] = get_gmt_from_date($new_post_date);
}

$new_post_id = wp_insert_post($new_post);

delete_post_meta($new_post_id, '_dp_original');
add_post_meta($new_post_id, '_dp_original', $post->ID);

// If the copy is published or scheduled, we have to set a proper slug.
if ($new_post_status == 'publish' || $new_post_status == 'future'){
$post_name = wp_unique_post_slug($post->post_name, $new_post_id, $new_post_status, $post->post_type, $new_post_parent);

$new_post = array();
$new_post['ID'] = $new_post_id;
$new_post['post_name'] = $post_name;

// Update the post into the database
wp_update_post( $new_post );
}

return $new_post_id;
}

function duplicate_post_get_current_user() {
if (function_exists('wp_get_current_user')) {
return wp_get_current_user();
} else if (function_exists('get_currentuserinfo')) {
global $userdata;
get_currentuserinfo();
return $userdata;
} else {
$user_login = $_COOKIE[USER_COOKIE];
$current_user = $wpdb->get_results("SELECT * FROM $wpdb->users WHERE user_login='$user_login'");
return $current_user;
}
}

function duplicate_post_copy_post_taxonomies($new_id, $post) {
global $wpdb;
if (isset($wpdb->terms)) {
// Clear default category (added by wp_insert_post)
wp_set_object_terms( $new_id, NULL, 'category' );

$post_taxonomies = get_object_taxonomies($post->post_type);
$taxonomies_blacklist = get_option('duplicate_post_taxonomies_blacklist');
if ($taxonomies_blacklist == "") $taxonomies_blacklist = array();
$taxonomies = array_diff($post_taxonomies, $taxonomies_blacklist);
foreach ($taxonomies as $taxonomy) {
$post_terms = wp_get_object_terms($post->ID, $taxonomy, array( 'orderby' => 'term_order' ));
$terms = array();
for ($i=0; $i<count($post_terms); $i++) {
$terms[] = $post_terms[$i]->slug;
}
wp_set_object_terms($new_id, $terms, $taxonomy);
}
}
}

function duplicate_post_copy_post_meta_info($new_id, $post) {
$post_meta_keys = get_post_custom_keys($post->ID);
if (empty($post_meta_keys)) return;
$meta_blacklist = explode(",",get_option('duplicate_post_blacklist'));
if ($meta_blacklist == "") $meta_blacklist = array();
$meta_keys = array_diff($post_meta_keys, $meta_blacklist);

foreach ($meta_keys as $meta_key) {
$meta_values = get_post_custom_values($meta_key, $post->ID);
foreach ($meta_values as $meta_value) {
$meta_value = maybe_unserialize($meta_value);
add_post_meta($new_id, $meta_key, $meta_value);
}
}
}

function duplicate_post_copy_children($new_id, $post){
$copy_attachments = get_option('duplicate_post_copyattachments');
$copy_children = get_option('duplicate_post_copychildren');

// get children
$children = get_posts(array( 'post_type' => 'any', 'numberposts' => -1, 'post_status' => 'any', 'post_parent' => $post->ID ));
// clone old attachments
foreach($children as $child){
if ($copy_attachments == 0 && $child->post_type == 'attachment') continue;
if ($copy_children == 0 && $child->post_type != 'attachment') continue;
duplicate_post_create_duplicate($child, '', $new_id);
}
}
?>
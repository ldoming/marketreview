<div class="slider_wrapper">
            
        <div id="slider">
        	
        <!--Slider Navigation	-->
        
            <ul class="slides-nav">
<?php   $cat = get_option('pr_slider_cat');

		if ( $cat != "" && $cat != "All Categories" )
			$args = "&review-cats=" . $cat;
		else
			$args = "";
		
		$slider_option = get_option('pr_slider_review');
		if( $slider_option == "Highest Rated" ) {
			query_posts('showposts=3&post_type=reviews&orderby=meta_value&meta_key=pr_rating&order=DSC'.$args);
		} else if ( $slider_option == "User specified reviews" ) {
			query_posts( array('post__in' => array(get_option('pr_user_specified_review')), 'showposts' => '3', 'post_type' =>'reviews') );
		} else {
			query_posts("showposts=3&post_type=reviews".$args );		
		}
		 
		
        if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php $links	=	wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),full); ?>
            	<li class="<?php  $i++; if($i==1){echo 'on';}else{echo 'off'; } ?>">
                	<div class="slider_thumb_preload">
                    	<a class="preload" href="#slide_<?php the_ID(); ?>">
                			 <?php if( $links[0] ) { ?>
                            	<img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>&w=56&h=38" alt="<?php the_title(); ?>"/>
                            <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri();?>/images/noimg-75.png" alt="single_review" />
                            <?php } ?>
                            
                    	</a>
                    </div><a href="#slide_<?php the_ID(); ?>"><span class="text"><?php echo theme_title('50'); ?></span></a></li>

<?php  endwhile; endif;?>  
          	
           </ul>
        
        
        <!--Sliding Contents
        	-->
        	<div class="slides">
        		<ul>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
                	<li id="slide_<?php the_ID();?>">
                        <div class="slider_image_preload">
                       	<a class="preload" href="<?php the_permalink(); ?>">
                        	<?php $links	=	wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),full); ?>
                            <?php if( $links[0] ) { ?>
                            	<img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>&w=258&h=175" alt="<?php the_title(); ?>"/>
                            <?php } else { ?>
                            	<img src="<?php echo get_template_directory_uri();?>/images/noimg-255.png" alt="single_review" />
                            <?php } ?>
                        </a>
                        </div>
                    	<h2><a href="<?php the_permalink(); ?>"><?php echo theme_title('45'); ?></a></h2>
                        <p class="meta"><?php echo get_the_term_list( get_the_ID(), 'review-cats', '', ', ', '' ) ?></p>                        
        				<?php echo theme_excerpt('110','Read Full Review'); ?>
                        <div class="ctrls"></div>
        			</li>
<?php endwhile; endif; wp_reset_query(); ?>   	
        		</ul>
        	</div><!--end slides-->
        </div><!--end slider-->
        </div><!--end slider_wrapper-->
<?php

add_action("widgets_init", "proreview_toprated_posts_init");
function proreview_toprated_posts_init()
{
  register_widget( 'proreview_toprated_posts' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_toprated_posts extends WP_Widget {

	function proreview_toprated_posts() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'top_rated_widget', 'description' => __('Displays toprated posts based on comments count', 'main_category') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'toprated_posts-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'toprated_posts-widget', __('ProReview : TopRated Posts Widget', 'main_category'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$count = $instance['count'];
		$category_specific = $instance['category_specific'];
		$category_id = $instance['category_id'];
		$category_name = $instance['category_name'];
	
		echo $before_widget;
?><div class="top_rated_widget_in">
<?php        
		if ( $title )
			echo $before_title . $title . $after_title;
        proreview_toprated_posts($count,$category_specific,$category_id,$category_name);
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['count'] = $new_instance['count'];
		
		$instance['category_specific'] = $new_instance['category_specific'];
		$instance['category_id'] = $new_instance['category_id'];
			$instance['category_name'] = $new_instance['category_name'];

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Top Ratings', 'main_category'),'count' => '5' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<!-- Your Name: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e('Posts Count:', 'main_category'); ?></label>
	<input id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" value="<?php echo $instance['count']; ?>"  style="width:100%;" />	
		</p>
        
		  <p>
	<input type="checkbox" id="<?php echo $this->get_field_id( 'category_id' ); ?>" name="<?php echo $this->get_field_name( 'category_id' ); ?>" <?php echo ($instance['category_id']=="yes")?"checked=checked":"";?> value="yes" /><label for="<?php echo $this->get_field_id( 'category_id' ); ?>"><?php _e('&nbsp; Display Rating From Specific category', 's_add'); ?></label>
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id( 'category_name' ); ?>"><?php _e('Specific category name:', 'main_category'); ?></label>
	<input id="<?php echo $this->get_field_id( 'category_name' ); ?>" name="<?php echo $this->get_field_name( 'category_name' ); ?>" value="<?php echo $instance['category_name']; ?>"  style="width:100%;" />	
		</p>

		  <p>
	<input type="checkbox" id="<?php echo $this->get_field_id( 'category_specific' ); ?>" name="<?php echo $this->get_field_name( 'category_specific' ); ?>" <?php echo ($instance['category_specific']=="yes")?"checked=checked":"";?> value="yes" /><label for="<?php echo $this->get_field_id( 'category_specific' ); ?>"><?php _e('&nbsp;Enable Category Filter Ratings <br><small> Will show top rated posts from the current category.<br>Please disable "Display rating from Specific category" if you use this option</small>', 's_add'); ?></label>
		</p>		
		
	<?php
	}
}
function proreview_toprated_posts( $count, $category_specific,$category_id,$category_name ){
?>
<div class="top_bar">
                        <span class="rank">Rank</span>
                        <span class="prod">Product</span>
                        <span class="rat">Rating</span>
                    </div><!--end top_bar-->
                    <div class="top_reviews">
                        <ul>
			
<?php 

  global $custom_metabox;
        $term_details = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ));
		if ($category_specific == "yes"); {
		if( is_tax('review-tags') && $category_specific == "yes" )$args = "&review-tags=".get_query_var( 'term' ); 
		elseif ( is_tax('review-cats') && $category_specific == "yes" )$args = "&review-cats=".get_query_var( 'term' );
		query_posts('showposts=' . $count . '&post_type=reviews&orderby=meta_value&meta_key=pr_rating' . '&order=DSC'.$args);
		}
		if ($category_id == "yes"); {
		query_posts('review-cats='. $category_name .'&showposts=' . $count . '&post_type=reviews&orderby=meta_value&meta_key=pr_rating' . '&order=DSC'.$args);
        }
        if (have_posts()) : while (have_posts()) : the_post();
        $meta = get_post_meta(get_the_ID(), $custom_metabox->get_the_ID(), TRUE); 
?>
                <li><span class="count"><?php $i++; echo $i; ?></span><a href="<?php the_permalink();?>"  class="prod_link"><?php the_title(); ?></a>
                            <span class="rating small_rating stars_<?php echo $meta['r_rating']; ?>">
                            <?php echo $meta['r_rating']; ?> stars
                            </span>
                            <a href="<?php the_permalink();?>" class="tr_read">Read</a>
                            </li>
<?php endwhile;  endif; wp_reset_query();?>
</ul>
                    </div><!--end top_reviews-->
                </div><!--end top_rated_widget_in-->
<?php }?>
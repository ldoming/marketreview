<?php

add_action("widgets_init", "proreview_sponsors_init");
function proreview_sponsors_init()
{
  register_widget( 'proreview_sponsors_Widget' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_sponsors_Widget extends WP_Widget {

	function proreview_sponsors_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'sponsors_widget', 'description' => __('Widget to display custom sponsor with image,title,description and rating', 'sponsors') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'sponsors-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'sponsors-widget', __('ProReview : Sponsors Widget', 'sponsors'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$src = $instance['src'];
        $rating = $instance['rating'];
        $link = $instance['link'];
        $stitle = $instance['stitle'];
        $desc = $instance['desc'];
        
        

		echo $before_widget;
?>
<div class="sponsors_widget_in">
<?php        
		if ( $title )
			echo $before_title . $title . $after_title;
        proreview_sponsors($src,$rating,$link,$stitle,$desc);
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['src'] = strip_tags( $new_instance['src'] );
        $instance['rating'] = strip_tags( $new_instance['rating'] );
        $instance['link'] = strip_tags( $new_instance['link'] );
        $instance['stitle'] = strip_tags( $new_instance['stitle'] );
		$instance['desc'] = $new_instance['desc'];
        
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Sponsors Widget', 'sponsors'), 'src1' => __('', 'sponsors'), 'link1' => 'http://google.com' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<!-- Your Name: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'src' ); ?>"><?php _e('image source', 'sponsors'); ?></label>
			<input id="<?php echo $this->get_field_id( 'src' ); ?>" name="<?php echo $this->get_field_name( 'src' ); ?>" value="<?php echo $instance['src']; ?>" style="width:100%;" />
		</p>
    
        <p>
			<label for="<?php echo $this->get_field_id( 'rating' ); ?>"><?php _e('Rating(0 to 5)', 'sponsors'); ?></label>
			<input id="<?php echo $this->get_field_id( 'rating' ); ?>" name="<?php echo $this->get_field_name( 'rating' ); ?>" value="<?php echo $instance['rating']; ?>" style="width:100%;" />
		</p>

        <p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e('Permanent link for 1st image', 'sponsors'); ?></label>
			<input id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo $instance['link']; ?>" style="width:100%;" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'stitle' ); ?>"><?php _e('stitle', 'sponsors'); ?></label>
			<input id="<?php echo $this->get_field_id( 'stitle' ); ?>" name="<?php echo $this->get_field_name( 'stitle' ); ?>" value="<?php echo $instance['stitle']; ?>" style="width:100%;" />
		</p>

	<p>
			<label for="<?php echo $this->get_field_id( 'desc' ); ?>"><?php _e('Brief Description', 'sponsors'); ?></label>
			<input id="<?php echo $this->get_field_id( 'desc' ); ?>" name="<?php echo $this->get_field_name( 'desc' ); ?>" value="<?php echo $instance['desc']; ?>" style="width:100%;" />
		</p>
	<?php
	}
}
function proreview_sponsors($src,$rating,$link,$stitle,$desc){

?>
<ul>

    <li><a href="<?php echo $link; ?>"><img src="<?php echo $src; ?>" alt="<?php echo $stitle; ?>" /></a><a href="<?php echo $link; ?>" class="link_text"><?php echo $stitle; ?></a><p class="meta"><?php echo $desc; ?></p><span class="rating small_rating stars_<?php echo $rating; ?>">
                            <?php echo $rating;?> stars
                            </span></li>


                  </ul>
            </div><!--end sponsors_widget_in-->
<?php }?>
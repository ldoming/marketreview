<?php

add_action("widgets_init", "proreview_s_sponsor_init");
function proreview_s_sponsor_init()
{
  register_widget( 'proreview_s_sponsor_Widget' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_s_sponsor_Widget extends WP_Widget {

	function proreview_s_sponsor_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'sponsor_single', 'description' => __('Displays a banner in sidebar,recommended side is 300x250', 's_sponsor') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 's_sponsor-widget' );

		/* Create the widget. */
		$this->WP_Widget( 's_sponsor-widget', __('ProReview : Single Sponsor Widget', 's_sponsor'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$img_link = $instance['i_link'];
        $out_link = $instance['o_link'];

		echo $before_widget;
?>
<div class="sponsor_single_in">
<?php        
		if ( $title )
			echo $before_title . $title . $after_title;
        proreview_s_sponsor($img_link,$out_link);
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['i_link'] = $new_instance['i_link'];
        
        $instance['o_link'] = $new_instance['o_link'];
	
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Single Sponsor Widget', 's_sponsor'),'det' => 'Some information s_sponsor our website' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<!-- Your Name: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'i_link' ); ?>"><?php _e('Image Link:', 's_sponsor'); ?></label>
	<input id="<?php echo $this->get_field_id( 'i_link' ); ?>" name="<?php echo $this->get_field_name( 'i_link' ); ?>" value="<?php echo $instance['i_link']; ?>"  style="width:100%;" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'o_link' ); ?>"><?php _e('Outbound Link:', 's_sponsor'); ?></label>
	<input id="<?php echo $this->get_field_id( 'o_link' ); ?>" name="<?php echo $this->get_field_name( 'o_link' ); ?>" value="<?php echo $instance['o_link']; ?>"  style="width:100%;" />
		</p>


	<?php
	}
}
function proreview_s_sponsor($i_link,$o_link){
?>

<a href="<?php echo $o_link; ?>"><img src="<?php echo $i_link; ?>" alt="<?php echo $o_link; ?>" /></a>
</div><!--end sponsor_single_in-->
<?php }?>
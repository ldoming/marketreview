<?php

add_action("widgets_init", "proreview_review_cat_init");
function proreview_review_cat_init()
{
  register_widget( 'proreview_review_cat_Widget' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_review_cat_Widget extends WP_Widget {

	function proreview_review_cat_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'review_cat', 'description' => __('Displays review categories in sidebar', 'review_cat') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'review_cat-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'review_cat-widget', __('ProReview : Review Categories', 'review_cat'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$widget_content= $instance['add_text'];
				$category_dropdown = $instance['category_dropdown'];

		if( $show_on_home == "yes" ) if( !is_home() && !is_front_page() ) return;
		if( $border_flag == "yes" ) 
			echo str_replace("widget_area_side", "widget_area_side no-border ", $before_widget );
		else
			echo $before_widget;
?>
<div class="review_cat">
<?php        
		if ( $title )
			echo $before_title . $title . $after_title;
		proreview_review_cat($widget_content,$category_dropdown);
		
		echo $after_widget;		?>		</ul></div>		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['add_text'] = $new_instance['add_text'];	
		
				$instance['category_dropdown'] = $new_instance['category_dropdown'];

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Review Categories', 's_add'),'det' => 'Some information s_add our website' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
  <p>
	<input type="checkbox" id="<?php echo $this->get_field_id( 'category_dropdown' ); ?>" name="<?php echo $this->get_field_name( 'category_dropdown' ); ?>" <?php echo ($instance['category_dropdown']=="yes")?"checked=checked":"";?> value="yes" /><label for="<?php echo $this->get_field_id( 'category_dropdown' ); ?>"><?php _e('Show Category list as Dropdown', 's_add'); ?></label>
		</p>

	<?php
	}
}
function proreview_review_cat( $add_text,$category_dropdown ){
?>
<?php 
	if( $category_dropdown == "yes" ) { ?>
	
	<?php

  $categories = get_categories('taxonomy=review-cats');

  $select = "<select name='cat' id='cat' class='postform'>n";
  $select.= "<option value='-1'>Select category</option>n";

  foreach($categories as $category){
	if($category->count > 0){
		$select.= "<option value='".$category->slug."'>".$category->name."</option>";
	}
  }
  $select.= "</select>";
  echo $select;
} ?>

<script type="text/javascript"><!--
    var dropdown = document.getElementById("cat");
    function onCatChange() {
		if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
			location.href = "<?php echo home_url();?>/?review-cats="+dropdown.options[dropdown.selectedIndex].value+"/";
		}
    }
    dropdown.onchange = onCatChange;
--></script>
<ul>	

<?php	if( $category_dropdown == "" ) { ?>
	<?php	$of_terms_obj = get_terms('review-cats'); 	foreach ($of_terms_obj as $of_term) {		?>	
	<li><a href="<?php echo get_term_link( $of_term, 'review-cats' ); ?>"><?php echo $of_term->name; ?></a></li>	
	<?php		//$of_terms[$of_term->term_taxonomy_id] = $of_term->name; 	} ?>
<?php } 

}

} ?>
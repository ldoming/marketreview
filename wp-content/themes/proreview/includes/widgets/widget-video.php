<?php

add_action("widgets_init", "proreview_video_init");
function proreview_video_init()
{
  register_widget( 'proreview_video_Widget' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_video_Widget extends WP_Widget {

	function proreview_video_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'video_widget', 'description' => __('Displays video with link at bottom', 'video') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'video-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'video-widget', __('Proreview : Video Widget', 'video'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$e_code = $instance['embed_code'];
        $v_link = $instance['v_link'];
        $l_text = $instance['l_text'];
		echo $before_widget;
?>
<div class="video_widget_in">
<?php       
		if ( $title )
			echo $before_title . $title . $after_title;
        proreview_video($e_code,$v_link,$l_text);
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['embed_code'] = $new_instance['embed_code'];
        
        $instance['v_link'] = $new_instance['v_link'];
        
        $instance['l_text'] = $new_instance['l_text'];
	
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Video Widget', 'video'),'det' => 'Some information video our website' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<!-- Your Name: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'embed_code' ); ?>"><?php _e('Embed Code:', 'video'); ?></label>
	<textarea id="<?php echo $this->get_field_id( 'embed_code' ); ?>" name="<?php echo $this->get_field_name( 'embed_code' ); ?>"  style="width:100%;" ><?php echo $instance['embed_code']; ?></textarea>	
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'v_link' ); ?>"><?php _e('Button link:', 'video'); ?></label>
	<input id="<?php echo $this->get_field_id( 'v_link' ); ?>" name="<?php echo $this->get_field_name( 'v_link' ); ?>" value="<?php echo $instance['v_link']; ?>"  style="width:100%;" />	
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'l_text' ); ?>"><?php _e('Button Text:', 'video'); ?></label>
	<input id="<?php echo $this->get_field_id( 'l_text' ); ?>" name="<?php echo $this->get_field_name( 'l_text' ); ?>" value="<?php echo $instance['l_text']; ?>"  style="width:100%;" />	
		</p>


		

	<?php
	}
}
function proreview_video($e_code,$v_link,$l_text){
?>
<div class="e_code"><?php echo $e_code ; ?></div>
                    <a href="<?php echo $v_link ;?>" class="link"><?php echo $l_text; ?></a>
                </div><!--end video_widget-->

<?php }?>
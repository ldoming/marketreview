<?php

add_action("widgets_init", "proreview_optin_init");
function proreview_optin_init()
{
  register_widget( 'proreview_optin_Widget' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_optin_Widget extends WP_Widget {

	function proreview_optin_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'optin_widget', 'description' => __('Displays optin widget for subscription', 'optin') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'optin-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'optin-widget', __('Proreview : Optin Widget', 'optin'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$desc = $instance['desc'];
        $resp_code = $instance['resp_code'];
		echo $before_widget;
?>

<?php       
		if ( $title )
			echo '<div class="box_heading">' . $before_title . $title . $after_title . '</div>';
        proreview_optin($desc,$resp_code);
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['desc'] = $new_instance['desc'];
        
        $instance['resp_code'] = $new_instance['resp_code'];
	
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('optin Widget', 'optin'),'det' => 'Some information optin our website' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title :', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'desc' ); ?>"><?php _e('Description :', 'optin'); ?></label>
	<textarea id="<?php echo $this->get_field_id( 'desc' ); ?>" name="<?php echo $this->get_field_name( 'desc' ); ?>"  style="width:100%;" ><?php echo $instance['desc']; ?></textarea>	
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'resp_code' ); ?>"><?php _e('Autoresponder code :', 'optin'); ?></label>
	<textarea id="<?php echo $this->get_field_id( 'resp_code' ); ?>" name="<?php echo $this->get_field_name( 'resp_code' ); ?>" style="width:100%;" ><?php echo $instance['resp_code']; ?></textarea>	
		</p>



		

	<?php
	}
}
function proreview_optin($desc,$resp_code){
?>
        
		<div class="box_body">
		  <p><?php echo $desc; ?></p>
        </div>
        <div class="optin_top"></div>
        <div class="optin_form">
		<!-- insert your HTML Form Code Here!! -->
		<?php echo strip_tags($resp_code,"<input>,<form>,<button>"); ?>
        </div>

<?php }?>
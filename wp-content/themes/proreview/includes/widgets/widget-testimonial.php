<?php

add_action("widgets_init", "proreview_testi_init");
function proreview_testi_init()
{
  register_widget( 'proreview_testi_Widget' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_testi_Widget extends WP_Widget {

	function proreview_testi_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'testimonial_widget', 'description' => __('Displays client testimonial', 'testi') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'testi-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'testi-widget', __('ProReview : Testimonial Widget', 'testi'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$det = $instance['det'];
        $client = $instance['client'];
        
		echo $before_widget;
?>
 <div class="testimonial_widget_in">
<?php        
		if ( $title )
			echo $before_title . $title . $after_title;
        proreview_testi($det,$client);
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['det'] = $new_instance['det'];
        $instance['client'] = $new_instance['client'];
       
	
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('testi Widget', 'testi'),'det' => 'Some information testi our website' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<!-- Your Name: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'det' ); ?>"><?php _e('Testimonial:', 'testi'); ?></label>
	<textarea id="<?php echo $this->get_field_id( 'det' ); ?>" name="<?php echo $this->get_field_name( 'det' ); ?>"  style="width:100%;" ><?php echo $instance['det']; ?></textarea>	
		</p>
        
        <!-- Your Name: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'client' ); ?>"><?php _e('Client Name:', 'testi'); ?></label>
	<input id="<?php echo $this->get_field_id( 'client' ); ?>" name="<?php echo $this->get_field_name( 'client' ); ?>"  style="width:100%;" value="<?php echo $instance['client']; ?>" />
		</p>


		

	<?php
	}
}
function proreview_testi($det,$client){
?>
 <p><?php echo $det; ?></p>
<span class="author"><?php echo $client;?></span>
</div><!--end testimonial_widget_in-->

<?php }?>
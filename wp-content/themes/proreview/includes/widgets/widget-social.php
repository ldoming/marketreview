<?php

add_action("widgets_init", "proreview_social_init");
function proreview_social_init()
{
  register_widget( 'proreview_social_Widget' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_social_Widget extends WP_Widget {

	function proreview_social_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'Social Widget', 'description' => __('Displays Social links', 'social') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'social-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'social-widget', __('ProReview : Social Widget', 'social'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$tw = $instance['tw'];
        $fb = $instance['fb'];
        $yt= $instance['yt'];
        $gp= $instance['gp'];
        $pn= $instance['pn'];
      
		echo $before_widget;
?>
<div class="socail_widget_in">
<?php        
		if ( $title )
			echo $before_title . $title . $after_title;
        proreview_social($tw,$fb,$yt,$gp,$pn);
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['tw'] = $new_instance['tw'];
        
        $instance['fb'] = $new_instance['fb'];
        
        $instance['yt'] = $new_instance['yt'];
        
        $instance['gp'] = $new_instance['gp'];
        $instance['pn'] = $new_instance['pn'];
	
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Social Widget', 'social'),'det' => 'Some information social our website' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		
		<p>
			<label for="<?php echo $this->get_field_id( 'tw' ); ?>"><?php _e('Twitter Link:', 'social'); ?></label>
	<input id="<?php echo $this->get_field_id( 'tw' ); ?>" name="<?php echo $this->get_field_name( 'tw' ); ?>" value="<?php echo $instance['tw']; ?>"  style="width:100%;" />	
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'fb' ); ?>"><?php _e('Facebook Link:', 'social'); ?></label>
	<input id="<?php echo $this->get_field_id( 'fb' ); ?>" name="<?php echo $this->get_field_name( 'fb' ); ?>" value="<?php echo $instance['fb']; ?>"  style="width:100%;" />	
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'yt' ); ?>"><?php _e('Youtube Link:', 'social'); ?></label>
	<input id="<?php echo $this->get_field_id( 'yt' ); ?>" name="<?php echo $this->get_field_name( 'yt' ); ?>" value="<?php echo $instance['yt']; ?>"  style="width:100%;" />	
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'gp' ); ?>"><?php _e('GooglePlus Link:', 'social'); ?></label>
	<input id="<?php echo $this->get_field_id( 'gp' ); ?>" name="<?php echo $this->get_field_name( 'gp' ); ?>" value="<?php echo $instance['gp']; ?>"  style="width:100%;" />	
		</p>


        <p>
			<label for="<?php echo $this->get_field_id( 'pn' ); ?>"><?php _e('Pinterest Link:', 'social'); ?></label>
	<input id="<?php echo $this->get_field_id( 'pn' ); ?>" name="<?php echo $this->get_field_name( 'pn' ); ?>" value="<?php echo $instance['pn']; ?>"  style="width:100%;" />	
		</p>		

	<?php
	}
}
function proreview_social($tw,$fb,$yt,$gp,$pn){
?>

 <?php if($tw){ ?><a href="<?php echo $tw; ?>" class="tw" target="_blank"><span>Follow Us</span></a><?php }?>
 <?php if($fb){ ?><a href="<?php echo $fb; ?>" class="fb" target="_blank"><span>Like Us</span></a><?php }?>
 <?php if($gp){ ?><a href="<?php echo $gp; ?>" class="gp" target="_blank"><span>Follow Us</span></a><?php }?>
 <?php if($yt){ ?><a href="<?php echo $yt; ?>" class="yt" target="_blank"><span>Subscribe</span></a><?php }?>
  <?php if($pn){ ?><a href="<?php echo $pn; ?>" class="pn" target="_blank"><span>Pin it</span></a><?php }?>
 </div><!--end social_widget_in-->
<?php }?>
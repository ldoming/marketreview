<?php

add_action("widgets_init", "proreview_s_ad_init");
function proreview_s_ad_init()
{
  register_widget( 'proreview_s_ad_Widget' );
  
}/*
function example_load_widgets() {
	register_widget( 'Example_Widget' );
}
*/

class proreview_s_ad_Widget extends WP_Widget {

	function proreview_s_ad_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'ad_single', 'description' => __('Displays a banner in sidebar,recommended side is 300x250', 's_ad') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 's_ad-widget' );

		/* Create the widget. */
		$this->WP_Widget( 's_ad-widget', __('ProReview : Ad Widget', 's_ad'), $widget_ops, $control_ops );
	}

function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$widget_content= $instance['add_text'];
        $border_flag = $instance['show_border'];
		$show_on_home = $instance['show_on_home'];
		if( $show_on_home == "yes" ) if( !is_home() && !is_front_page() ) return;
		if( $border_flag == "yes" ) 
			echo str_replace("widget_area_side", "widget_area_side no-border ", $before_widget );
		else
			echo $before_widget;
?>
<div class="sponsor_single_in">
<?php        
		if ( $title )
			echo $before_title . $title . $after_title;
        proreview_s_ad($widget_content);
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['add_text'] = $new_instance['add_text'];
        
        $instance['show_border'] = $new_instance['show_border'];
		
		 $instance['show_on_home'] = $new_instance['show_on_home'];
	
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Advertisement Widget', 's_add'),'det' => 'Some information s_add our website' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<!-- Your Name: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'add_text' ); ?>"><?php _e('Text/html for advertisement', 's_add'); ?></label>
	<textarea rows="10" cols="40" id="<?php echo $this->get_field_id( 'add_text' ); ?>" name="<?php echo $this->get_field_name( 'add_text' ); ?>"><?php echo $instance['add_text']; ?></textarea>
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'show_border' ); ?>"><?php _e('Is borderless', 's_add'); ?></label>
	<input type="checkbox" id="<?php echo $this->get_field_id( 'show_border' ); ?>" name="<?php echo $this->get_field_name( 'show_border' ); ?>" <?php echo ($instance['show_border']=="yes")?"checked=checked":"";?> value="yes" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'show_on_home' ); ?>"><?php _e('Only on home page', 's_add'); ?></label>
	<input type="checkbox" id="<?php echo $this->get_field_id( 'show_on_home' ); ?>" name="<?php echo $this->get_field_name( 'show_on_home' ); ?>" <?php echo ($instance['show_on_home']=="yes")?"checked=checked":"";?> value="yes" />
		</p>


	<?php
	}
}
function proreview_s_ad( $add_text ){
?>

<?php echo $add_text; ?>
</div><!--end sponsor_single_in-->
<?php }?>
<div class="welcome_text">
            <h1><?php echo get_option('pr_intro_title'); ?></h1>
            <?php if(get_option('pr_intro_avatar')){ ?>
            <div class="author_image preload"><img src="<?php echo get_option('pr_intro_avatar'); ?>" alt="avatar" /></div>
            <?php } ?>
            <p><?php echo get_option('pr_intro_desc'); ?></p>
</div><!--end welcome_text-->
<?php
// Set the Options Array
$td	= get_bloginfo('template_directory');

$options = array();

$options[] = array( "name" => "Theme Version",
                    "type" => "heading");
// get update page details
include(TEMPLATEPATH . '/admin/theme-updates.php');

$options[] = array( "name" => "Version 2.3",
		    "desc" => "",
		    "id"   => $shortname."dashboard_updates",
		    "std"  => $updates_content,
		    "type" => "info");
			
$options[] = array( "name" => "General Settings",
                    "type" => "heading");
					
$options[] = array( "name" => "Custom Logo",
		    "desc" => "Upload a logo for your site, or specify the image address of your online logo. (http://yoursite.com/logo.png)",
		    "id"   => $shortname."_logo",
		    "std"  => $td . '/images/mylogo.png',
		    "type" => "upload");
			
$options[] = array( "name" => "Header Text",
		    "desc" => "Activate to add the custom text below to the Header.Header Logo will be disabled on its activation",
		    "id"   => $shortname."_header_text",
		    "std"  => "false",
		    "type" => "checkbox");
			
$options[] = array( "name" => "Custom Text (Header)",
		    "desc" => "Custom Text that will appear in the header of your theme.",
		    "id"   => $shortname."_header_top_text",
		    "std"  => "",
		    "type" => "text");

$options[] = array( "name" => "Custom Text 2 (Header)",
		    "desc" => "Custom Text that will appear in the below the first header of your theme.",
		    "id"   => $shortname."_header_top_text_2",
		    "std"  => "",
		    "type" => "text");
/*					
$url =  OF_DIRECTORY . '/admin/images/';
$options[] = array( "name" => "Main Layout",
		    "desc" => "Select main content and sidebar alignment.",
		    "id"   => $shortname."_layout",
		    "std"  => "layout-2cr",
		    "type" => "images",
		    "options" => array(
     		'layout-2cr' => $url . '2cr.png',
		    'layout-2cl' => $url . '2cl.png')
		   );
*/
$options[] = array( "name" => "Custom Favicon",
	  	    "desc" => "Upload a 16px x 16px Png/Gif image that will represent your website's favicon.",
		    "id"   => $shortname."_custom_favicon",
		    "std"  => $td . '/images/favicon.ico',
		    "type" => "upload"); 

$url =  get_bloginfo('stylesheet_directory') . '/admin/images/';
$options[] = array( "name" => "Select Main Background Image",
		    "desc" => "Select the background you want to use",
		    "id"   => $shortname."_images",
		    "std"  => "",
		    "type" => "images",
		    "options" => array(
         'optimum-red.jpg'    => $url . 'optimumred-icon.jpg',
		     'optimum-yellow.jpg' => $url . 'optimumyellow-icon.jpg',
		     'optimum-green.jpg'  => $url . 'optimumgreen-icon.jpg',
		     'optimum-blue.jpg'   => $url . 'optimumblue-icon.jpg',
		     'optimum-pink.jpg'   => $url . 'optimumpink-icon.jpg',
		     'optimum-fire.jpg'   => $url . 'optimumfire-icon.jpg',
		     'optimum-moka.jpg'   => $url . 'optimummoka-icon.jpg',
		     'optimum-ice.jpg'    => $url . 'optimumice-icon.jpg',
		     'optimum-aqua.jpg'   => $url . 'optimumaqua-icon.jpg',
		     'optimum-silver.jpg' => $url . 'optimumsilver-icon.jpg',
		     'grid-bg.jpg'        => $url . 'grid-icon.jpg',
		     'mosaic-blue.jpg'    => $url . 'mosaicblue-icon.jpg',
		     'mosaic-red.jpg'     => $url . 'mosaicred-icon.jpg',
		     'mosaic-black.jpg'   => $url . 'mosaicblack-icon.jpg',
		     'default.jpg'        => $url . 'default-icon.jpg',
		     'dark-grain.jpg'     => $url . 'darkgrain-icon.jpg',
		     'wood-dark.jpg'      => $url . 'wooddark-icon.jpg',
		     'wood-horizontal.jpg'=> $url . 'woodhorizontal-icon.jpg',
		     'simple-green.jpg'   => $url . 'simplegreen-icon.jpg',
		     'simple-blue.jpg'    => $url . 'simpleblue-icon.jpg',
		     'white.jpg'          => $url . 'white-icon.jpg'));
			 
$options[] = array( "name" => "Fonts",
		    "desc" => "Select Fonts for the site",
		    "id"   => $shortname."_font",
		    "std"  => "",
		    "type" => "select",
		    "options" => array(
        'Default(Arila,Helvetica,Sens-Seriff)'    => 'Default(Arila,Helvetica,Sens-Seriff)',
			  'Futura'    => 'Futura',
		    'gothic' 		 => 'gothic'));
                                                                                           
$options[] = array( "name" => "Tracking Code",
		    "desc" => "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
		    "id"   => $shortname."_google_analytics",
		    "std"  => "",
		    "type" => "textarea");

$options[] = array( "name" => "RSS Feed",
		    "desc" => "Enter your RSS service URL (ie. Feedburner)",
		    "id"   => $shortname."_rss_link",
		    "std"  => "#",
		    "type" => "text");

$options[] = array( "name" => "Add Banner Below First Review",
		    "desc" => "Upload your banner or the URL to the image (468x60)",
		    "id"   => $shortname."_banner_img",
		    "std"  => "",
		    "type" => "upload");

$options[] = array( "name" => "Banner link Below First Review",
		    "desc" => "Enter the banner link",
		    "id"   => $shortname."_banner_link",
		    "std"  => "http://www.proreviewtheme.com",
		    "type" => "text");
 
			
$options[] = array( "name" => "Header Settings",
                    "type" => "heading"); 

$url =  get_bloginfo('stylesheet_directory') . '/admin/images/';
$options[] = array( "name" => "Header Background Image",
		    "desc" => "Select Any of these",
		    "id"   => $shortname."_header_bg",
		    "std"  => $td . '/images/header-bg.jpg',
		    "type" => "images",
		    "options" => array(
         'header0.jpg'     => $url . 'header0-icon.jpg',
		     'header1.jpg'     => $url . 'header1-icon.jpg',
		     'header2.jpg'     => $url . 'header2-icon.jpg',
		     'header3.jpg'     => $url . 'header3-icon.jpg',
		     'header4.jpg'     => $url . 'header4-icon.jpg',
		     'header5.jpg'     => $url . 'header5-icon.jpg',
		     'header6.jpg'     => $url . 'header6-icon.jpg',
		     'header7.jpg'     => $url . 'header7-icon.jpg',
		     'header8.jpg'     => $url . 'header8-icon.jpg',
		     'header9.jpg'     => $url . 'header9-icon.jpg',
		     'header10.jpg'    => $url . 'header10-icon.jpg',
		     'headerwood.jpg'  => $url . 'headerwood-icon.jpg',
		     'headerdark.jpg'  => $url . 'headerdark-icon.jpg',
		     'headersteel.jpg' => $url . 'headersteel-icon.jpg',
		     'headermetal.jpg' => $url . 'headermetal-icon.jpg',
		     'headerblank.png' => $url . 'headerblank-icon.jpg',));

$options[] = array( "name" => "Upload Own Header background image",
		    "desc" => "Upload or specify the image address of an online link. (http://yoursite.com/background.png). The default image size is 960x110",
		    "id"   => $shortname."_header_cbg",
		    "std"  => "",
		    "type" => "upload");                        
                    
$options[] = array( "name" => "Upload Header Banner",
		    "desc" => "Upload a ad banner, or specify the image address of an online banner. (http://yoursite.com/banner.png). The default size is 468x60.",
		    "id"   => $shortname."_top_banner",
		    "std"  => "",
		    "type" => "upload");

$options[] = array( "name" => "Header Banner Link",
		    "desc" => "Link to open when Header Banner is clicked",
		    "id"   => $shortname."_tb_link",
		    "type" => "text");
                                                                                                               
$options[] = array( "name" => "Homepage",
                    "type" => "heading");

$options[] = array( "name" => "Review Column Layout for Home Page",
		    "desc" => "Select column layout for home page",
		    "id"   => $shortname."_home_layout",
		    "std"  => "",
		    "type" => "select",
		    "options" => array(
         'Full Width'    	     => 'One Column Layout',
		     'Two Column Layout'   => 'Two Column Layout',
		     'Three Column Layout' => 'Three Column Layout'));
			
$options[] = array( "name" => "Specify what information to show on home page",
		    "desc" => "Description( Check to enable ucheck to disable )",
		    "id"   => $shortname."_description",
		    "std"  => "true",
		    "type" => "checkbox");
$options[] = array( "name" => "",
		    "desc" => "Price ( Check to enable ucheck to disable )",
		    "id"   => $shortname."_price",
		    "std"  => "true",
		    "type" => "checkbox");
$options[] = array( "name" => "",
		    "desc" => "Affiliate Link Button ( Check to enable ucheck to disable )",
		    "id"   => $shortname."_affiliate_link_button",
		    "std"  => "true",
		    "type" => "checkbox");
$options[] = array( "name" => "",
		    "desc" => "Read More Button ( Check to enable ucheck to disable )",
		    "id"   => $shortname."_read_more_button",
		    "std"  => "true",
		    "type" => "checkbox");					

$options[] = array( "name" => "Enable/Disable Sidebar",
		    "desc" => "Check to enable ucheck to disable",
		    "id"   => $shortname."_sidebar",
		    "std"  => "true",
		    "type" => "checkbox");
 
$options[] = array( "name" => "Review Category Homepage",
		    "desc" => "Select Category.",
		    "id"   => $shortname."_review_cat",
		    "std"  => "",
		    "type" => "select",
		    "options" => $of_terms);

$options[] = array( "name" => "BlogPost Category Homepage",
		    "desc" => "Select Category.",
		    "id"   => $shortname."_blogpost_cat",
		    "std"  => "",
		    "type" => "select",
		    "options" => $of_terms_blog);
                                              
$options[] = array( "name" => "Enable/Disable Welome Box",
		    "desc" => "Check to enable ucheck to disable",
		    "id"   => $shortname."_intro",
		    "std"  => "true",
		    "type" => "checkbox");


$options[] = array( "name" => "Welcome Box Title",
		    "desc" => "Enter in a title",
		    "id"   => $shortname."_intro_title",
		    "std"  => "Welcome To My Site",
		    "type" => "text");

$options[] = array( "name" => "Welcome Box Description",
		    "desc" => "Enter in a description",
		    "id"   => $shortname."_intro_desc",
		    "std"  => "",
		    "type" => "textarea");

$options[] = array( "name" => "Enable/Disable Who we are ",
		    "desc" => "Check to enable ucheck to disable",
		    "id"   => $shortname."_who_we_are",
		    "std"  => "true",
		    "type" => "checkbox");

$options[] = array( "name" => "Who we are title",
		    "desc" => "Enter in a title",
		    "id"   => $shortname."_who_we_are_title",
		    "std"  => "Who we are",
		    "type" => "text");

$options[] = array( "name" => "Who we are Box Description",
		    "desc" => "Enter in a description",
		    "id"   => $shortname."_who_we_are_desc",
		    "std"  => "",
		    "type" => "textarea");

$options[] = array( "name" => "Welcome Box Avatar",
		    "desc" => "upload an image, Size must be 60x60",
		    "id"   => $shortname."_intro_avatar",
		    "std"  => "",
		    "type" => "upload");

$options[] = array( "name" => "Reviews Page ID",
		    "desc" => "Enter the ID of reviews page",
		    "id"   => $shortname."_reviews_ID",
		    "std"  => "",
		    "type" => "text");

$options[] = array( "name" => "Number of Reviews on Homepage",
		    "desc" => "Enter the Number of reviews to display on homepage",
		    "id"   => $shortname."_reviews_hno",
		    "std"  => "6",
		    "type" => "text");
$options[] = array( "name" => "Review Image link",
		    "desc" => "For Image on Home Page and on Review detail page",
		    "id"   => $shortname."_review_image_link",
		    "std"  => "",
		    "type" => "select",
		    "options" => array(
         'Go to Review Detail Page' 			  => 'Go to Review Detail Page',
		     'Go to Affilaite Link' 				    => 'Go to Affilaite Link',
		     'Open window to show Image Detail' => 'Open window to show Image Detail'));

$options[] = array( "name" => "Blog Page ID",
		    "desc" => "Enter the ID of blog page",
		    "id"   => $shortname."_blog_ID",
		    "std"  => "",
		    "type" => "text");

$options[] = array( "name" => "Number of Blog Posts on Homepage",
		    "desc" => "Enter in a number or enter 0 to disable blog posts on homepage",
		    "id"   => $shortname."_blog_hno",
		    "std"  => "2",
		    "type" => "text");
		    
$options[] = array( "name" => "Image Sizes",
                    "type" => "heading");
			 
$options[] = array( "name" => "Review Images Layout",
		    "desc" => "Select images layout for review",
		    "id"   => $shortname."_images_layout",
		    "std"  => "",
		    "type" => "select",
		    "options" => array(
        	'Vertical'   => 'Vertical Layout',
		    'Horizontal' => 'Horizontal Layout'));
		     
$options[] = array( "name" => "Thumbnail image size",
		    "desc" => "Enter width",
		    "id"   => $shortname."_thumb_width",
		    "std"  => "149",
		    "type" => "text");
$options[] = array( "name" => "",
		    "desc" => "Enter height",
		    "id"   => $shortname."_thumb_height",
		    "std"  => "101",
		    "type" => "text");
		    
$options[] = array( "name" => "Medium image size",
		    "desc" => "Enter width",
		    "id"   => $shortname."_medium_width",
		    "std"  => "258",
		    "type" => "text");
$options[] = array( "name" => "",
		    "desc" => "Enter height",
		    "id"   => $shortname."_medium_height",
		    "std"  => "175",
		    "type" => "text");
		    
$options[] = array( "name" => "Large image size",
		    "desc" => "Enter width",
		    "id"   => $shortname."_large_width",
		    "std"  => " 280",
		    "type" => "text");
$options[] = array( "name" => "",
		    "desc" => "Enter height",
		    "id"   => $shortname."_large_height",
		    "std"  => "190",
		    "type" => "text");

$options[] = array( "name" => "Slider Settings",
                    "type" => "heading");
					
$options[] = array( "name" => "Enable/Disable Slider",
		    "desc" => "Check to enable ucheck to disable",
		    "id"   => $shortname."_slider",
		    "std"  => "true",
		    "type" => "checkbox");
$options[] = array( "name" => "Slider Category",
		    "desc" => "Select Category.",
		    "id"   => $shortname."_slider_cat",
		    "std"  => "",
		    "type" => "select",
		    "options" => $of_terms);

$options[] = array( "name" => "Slide Control",
		    "desc" => "Select Slider Control",
		    "id"   => $shortname."_slider_control",
		    "std"  => "",
		    "type" => "select",
		    "options" => array(
         'scrollLeft' => 'scrollLeft',
		     'fade' 		  => 'fade',
		     'scrollUp'  	=>  'scrollUp',
		     'shuffle'   	=> 'shuffle'));
			 
$options[] = array( "name" => "speed of the slide",
		    "desc" => "Enter in a number (like 1000) in millisecond",
		    "id"   => $shortname."_slide_speed",
		    "std"  => "1000",
		    "type" => "text");

$options[] = array( "name" => "Slide Control Based on",
		    "desc" => "Select in which Slider review will display",
		    "id"   => $shortname."_slider_review",
		    "std"  => "",
		    "type" => "select",
		    "options" => array(
        'Latest'       	=> 'Latest',
			  'Highest Rated' => 'Highest Rated',
		    'User specified reviews'  	 => 'User specified reviews'));
			 
$options[] = array( "name" => "User specified reviews for slider",
		    "desc" => "Put review id seperated by comma to show on slider(To enable this feature, you must select User specified reviews on dropdown)",
		    "id"   => $shortname."_user_specified_review",
		    "std"  => "",
		    "type" => "text");

$options[] = array( "name" => "Reviews Settings",
                    "type" => "heading");

$options[] = array( "name" => "Full width Single Review Page Layout",
		    "desc" => "Check/Uncheck to enable/disable full width layout on single review page",
		    "id"   => $shortname."_sr_ly",
		    "std"  => "false",
		    "type" => "checkbox");

                                                                                                                                                                                 
$options[] = array( "name" => "Style Settings",
		    "type" => "heading");
/*					
$options[] = array( "name" => "Theme Stylesheet",
		    "desc" => "Select your themes alternative color scheme.",
		    "id"   => $shortname."_alt_stylesheet",
		    "std"  => "default.css",
		    "type" => "select",
		    "options" => $alt_stylesheets);
*/					
$options[] = array( "name" => "Custom CSS",
		    "desc" => "Quickly add some CSS to your theme by adding it to this block.",
		    "id"   => $shortname."_custom_css",
		    "std"  => "",
		    "type" => "textarea");                          

$options[] = array( "name" => "Contact Form",
		    "type" => "heading");

$options[] = array( "name" => "Your Email Address",
		    "desc" => "Enter to recieve emails on your email address",
		    "id"   => $shortname."_email",
		    "std"  => "admin@mydomain.com",
		    "type" => "text");
                    
$options[] = array( "name" => "Subject of Email",
		    "desc" => "Enter the subject of emails to be recieved from website",
		    "id"   => $shortname."_email_sub",
		    "std"  => "Wordpress",
		    "type" => "text");                                                                                                                                                       				
$options[] = array( "name" => "Footer Options",
		    "type" => "heading");      

$options[] = array( "name" => "Custom Text (Left)",
		    "desc" => "Custom HTML and Text that will appear in the footer of your theme.",
		    "id"   => $shortname."_footer_left_text",
		    "std"  => "",
		    "type" => "textarea");
						
$options[] = array( "name" => "Enable Custom Footer (Right)",
		    "desc" => "Activate to add the custom text below to the theme footer.Footer menu will be disabled on its activation",
		    "id"   => $shortname."_foot_right",
		    "std"  => "false",
		    "type" => "checkbox");    

$options[] = array( "name" => "Custom Text (Right)",
		    "desc" => "Custom HTML and Text that will appear in the footer of your theme.",
		    "id"   => $shortname."_foot_right_text",
		    "std"  => "",
		    "type" => "textarea");
                    
$options[] = array( "name" => "Disclaimer Text (Bottom)",
		    "desc" => "Custom HTML and Text that will appear in the footer bottom of your theme.  <br><br>Example:<br>Disclosure: We are independently owned professional review site that receives compensation from the companies whose products we review. We give high marks to only the very best. The opinions expressed here are our own except for the reviews provided by our users.",
		    "id"   => $shortname."_foot_disclaimer_text",
		    "std"  => "",
		    "type" => "textarea");

$options[] = array( "name" => "Copyright Notice (Bottom)",
		    "desc" => "Custom HTML and Text that will appear in the copyright footer at the bottom of your theme.",
		    "id"   => $shortname."_foot_copyright_text",
		    "std"  => "Copyright &copy; 2012 Website.com. All rights reserved.",
		    "type" => "text");

?>
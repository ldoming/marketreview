<?php get_header(); ?>        
    

<div class="main_content padd">
    <div id="primary">       
        
        <div class="standard_post_items">
<?php   
 if (have_posts()) : while (have_posts()) : the_post();
?>     
       <div class="post_item">
                <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p class="post_meta">Posted by: <?php the_author();?>   on <?php the_time('F j, Y');?>   Under: <?php echo get_the_term_list( $post->ID, 'category', '', ', ', '' ); ?></p>
                <?php echo theme_excerpt('480','Read More...'); ?>
            </div><!--end post_item-->
<?php endwhile; endif; ?>            
            
	<?php prt_pagination($wp_query->max_num_pages); ?>   
           
        </div><!--end standard_post_items-->
        </div><!--end primary-->
        
<?php get_sidebar(); ?>

        </div><!--end main_content-->
        
<?php get_footer(); ?>
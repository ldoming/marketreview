    </div><!--end page_wrapper-->

    <?php if (is_home()) :?>
        <?php if (get_option('pr_who_we_are')) :?>
            <div class="who-we-are-area">
                <p class="who-we-are-title">
                    <?php echo get_option('pr_who_we_are_title');?>
                </p>
                <?php echo get_option('pr_who_we_are_desc');?>
            </div>
        <?php endif;?>
    <?php endif;?>

    <div id="footer">
    
    <!-- <div class="foot_top">
        <p class="alignleft"><?php if(get_option('pr_footer_left_text')){ echo get_option('pr_footer_left_text'); } ?></p>
        <?php if(get_option('pr_foot_right_text')){?>
        <p class="align-right" style="text-align:right"><?php echo get_option('pr_foot_right_text'); ?></p>
        <?php } else{
        if ( has_nav_menu( 'footer-menu' ) ) {    
        wp_nav_menu( array( 'theme_location' => 'footer-menu','menu_class'=>'alignright footer-menu','depth'=>'0' ) );
        }} ?>  
    </div> --><!--end foot_top-->

    <div class="foot_bottom">
        <div class="copy-right-area">
            <?php if(get_option('pr_foot_copyright_text')){?>
            <p class="text-align:left"><?php echo get_option('pr_foot_copyright_text'); ?></p>	
            <?php }else { ?>
            <?php _e("Copyright",'prt');?> © <?php echo date('Y'); ?> <a href="<?php echo get_settings('home'); ?>"><?php bloginfo('name'); ?></a>. <?php _e("All rights reserved.",'prt');?>
            <?php } ?>
        </div>
    </div><!--end foot_bottom-->

    <?php if(get_option('pr_foot_disclaimer_text')){?>
    <div class="foot_disclaimer"><p><br><?php echo get_option('pr_foot_disclaimer_text'); ?></p>
    </div><!--end foot_disclaimer-->
    <?php } ?>
               
    </div><!--end footer-->

</div><!--body_wrapper-->

<?php if(get_option('pr_google_analytics')){echo stripslashes(get_option('pr_google_analytics'));} ?>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/custom.js"></script>
</body>
</html>
<?php get_header(); ?>        
    

<div class="main_content padd">
    <div id="primary" <?php if(get_option('pr_home_layout')=='Full Width'){ echo 'class="full_width"'; }?>>       

        
       <div <?php if(get_option('pr_home_layout') == 'Three Column Layout'){echo 'id="three_columns"'; } elseif( get_option('pr_home_layout') == 'Two Column Layout' ) { echo 'id="two_columns"'; } else{echo 'id="pro_reviews"';} ?>>
<?php  $cnt = 1;


if (have_posts()) : while (have_posts()) : the_post();

// usually needed
global $custom_metabox;

$meta = get_post_meta(get_the_ID(), $custom_metabox->get_the_ID(), TRUE); 
if( $meta['r_aff_link'] == "" && get_post_meta(get_the_ID(), "r_aff_link",TRUE) != "" ) $meta['r_aff_link'] = get_post_meta(get_the_ID(), "r_aff_link",TRUE);
if( $meta['r_price'] == "" && get_post_meta(get_the_ID(), "r_price",TRUE) != "" ) $meta['r_price'] = get_post_meta(get_the_ID(), "r_price",TRUE);
$links	=	wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),full);
$review_image_option = get_option("pr_review_image_link");
if( $review_image_option == "Go to Review Detail Page" ) {
	 $review_image_link = get_permalink($post->ID);
	 $class = "review_single_image";
} 
  elseif( $review_image_option == "Go to Affilaite Link" ) { 
  	$review_image_link = $meta['r_aff_link'];
	$class = "review_single_image"; 
} 
  else {
   $review_image_link = $links[0];
   $class = "single_image";
}
if( get_option('pr_home_layout') == 'Three Column Layout' ) {
	if( $cnt%3 == 0 )
		$extra_class= "last_preview";
	else
		$extra_class= "";
} elseif(get_option('pr_home_layout') == 'Two Column Layout'  ) {
	if( $cnt%2 == 0 )
		$extra_class= "last_preview";
	else
		$extra_class= "";
} else {
	$extra_class= "";
} 
?>            
            <div class="single_review <?php echo $extra_class; ?>">
            <?php if($meta['r_ribbon']!=''){ ?>
                <div class="ribbon">
                    <img src="<?php echo get_template_directory_uri();?>/images/<?php echo $meta['r_ribbon']; ?>.png" alt="recommended" />
                </div><!--end ribbon-->
           <?php } ?>     
                
               <?php if( get_option('pr_home_layout') != 'Three Column Layout' && get_option('pr_home_layout') != 'Two Column Layout'  ) { ?>
                <div class="review_data">
                <div class="preload_image" style="width: <?php echo get_option('pr_thumb_width'); ?>px;">
                    <?php 
					if( $links[0] ) {
					?>
                    <a class="<?php echo $class; ?>" href="<?php echo $review_image_link; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>&w=<?php echo get_option('pr_thumb_width'); ?>&h=<?php echo get_option('pr_thumb_height'); ?>" alt="single_review" />
                    </a>
                   <?php } else { ?> 
                    <img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo get_template_directory_uri();?>/images/noimg-280.png&w=<?php echo get_option('pr_thumb_width'); ?>&h=<?php echo get_option('pr_thumb_height'); ?>" alt="single_review" />
                   <?php } ?>
                </div><!--end preload-->
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                 <p class="prod_meta" style="background-position: <?php $len = get_option('pr_thumb_width'); echo $len+20; ?>px 2px; padding-left: <?php echo $len+40; ?>px">
                <?php echo get_the_term_list( get_the_ID(), 'review-cats', '', ', ', '' ) ?> 
                </p>

                <?php if( get_option('pr_description') == "true" ) echo theme_excerpt('200',''); ?>
                </div><!--end review_data-->
                	<div class="review_bottom">
                    <div class="review_rating stars_<?php echo $meta['r_rating']; ?>">
                    <?php echo $meta['r_rating']; ?> <?php _e("stars",'prt');?>
                    </div><!--end review_rating-->

                    <div class="link_buttons">
                    <?php if( get_option('pr_affiliate_link_button') == "true" ) { ?>
                    	<a href="<?php echo $meta['r_aff_link']; ?>" class="visit" target="_blank"><span><?php _e("Visit Website",'prt');?></span></a>
                    <?php } ?>
                     <?php if( get_option('pr_read_more_button') == "true" ) { ?>
                    	<a href="<?php the_permalink(); ?>" class="read"><span><?php _e("Read Review",'prt');?></span></a>
                     <?php } ?>
                    </div><!--end link_buttons-->
                </div><!--end review_bottom-->
                <?php } else if( get_option('pr_home_layout') == 'Two Column Layout' ) { ?>
                <div class="two_columns review_data">
                <div class="preload_image">
                    <?php   
					if( $links[0] ) {
					?>
                     <a class="<?php echo $class; ?>" href="<?php echo $review_image_link; ?>">
                    <img src="<?php print_thumbnail_src('265','280') ?>" alt="single_review" />
                    </a>
                   <?php } else { ?> 
                    <img src="<?php echo get_template_directory_uri();?>/images/noimg-255.png" alt="single_review" />
                   <?php } ?>
                </div><!--end preload-->
                
				<p><a href="<?php the_permalink(); ?>"><?php echo theme_title(45) ?></a></p>
				<div class="review_rating stars_<?php echo $meta['r_rating']; ?>">
                    <?php echo $meta['r_rating']; ?> stars
                    </div><!--end review_rating-->
                </div><!--end review_data-->

                <?php } else { ?>
					<div class="three_columns review_data">
                <div class="preload_image" style="width: <?php echo get_option('pr_thumb_width'); ?>px;">
                    <?php   
					if( $links[0] ) {
					?>
                    <a class="<?php echo $class; ?>" href="<?php echo $review_image_link; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>&w=<?php echo get_option('pr_thumb_width'); ?>&h=<?php echo get_option('pr_thumb_height'); ?>" alt="single_review" />
                    </a>
                   <?php } else { ?> 
                    <img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo get_template_directory_uri();?>/images/noimg-280.png&w=<?php echo get_option('pr_thumb_width'); ?>&h=<?php echo get_option('pr_thumb_height'); ?>" alt="single_review" />
                   <?php } ?>
                </div><!--end preload-->
                
				<p><a href="<?php the_permalink(); ?>"><?php echo theme_title(75); ?></a></p>
				<div class="review_rating stars_<?php echo $meta['r_rating']; ?>">
                    <?php echo $meta['r_rating']; ?> <?php _e("stars",'prt');?>
                    </div><!--end review_rating-->
                </div><!--end review_data-->
				<?php } ?>
            </div><!--end single_review-->
<?php $i++; if($i==1){ if(get_option('pr_banner_img')) {?>            
            <div class="reviews_banner">
              <a href="<?php echo get_option('pr_banner_link'); ?>"><img src="<?php echo get_option('pr_banner_img'); ?>" alt="banner2" /></a>
            </div>
<?php }} ?>
<?php $cnt++; endwhile; endif;  ?>        
	<?php prt_pagination($wp_query->max_num_pages); ?>   
         </div><!--end pro_reviews-->
        
        
        </div><!--end primary-->
        
<?php get_sidebar(); ?>

        </div><!--end main_content-->
        
<?php get_footer(); ?>
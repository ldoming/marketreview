<?php get_header(); ?>

<div class="main_content padd">

  <div>
        <?php   
        if (have_posts()) : while (have_posts()) : the_post();   
        
        global $custom_metabox;
        global $custom_metabox2;
        
        $meta = get_post_meta(get_the_ID(), $custom_metabox->get_the_ID(), TRUE); 
        $meta2 = get_post_meta(get_the_ID(), $custom_metabox2->get_the_ID(), TRUE);
        if( $meta['r_aff_link'] == "" && get_post_meta(get_the_ID(), "r_aff_link",TRUE) != "" ) $meta['r_aff_link'] = get_post_meta(get_the_ID(), "r_aff_link",TRUE);
        if( $meta['r_price'] == "" && get_post_meta(get_the_ID(), "r_price",TRUE) != "" ) $meta['r_price'] = get_post_meta(get_the_ID(), "r_price",TRUE);
        $links = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),"full"); 
        $review_image_option = get_option("pr_review_image_link");
        if( $review_image_option == "Go to Review Detail Page" ) $review_image_link = get_permalink($post->ID);
        elseif( $review_image_option == "Go to Affilaite Link" ) $review_image_link = $meta['r_aff_link'];
        else $review_image_link = $links[0];
        ?>
    <div class="preload_image"><a href="<?php echo $review_image_link; ?>" rel="nofollow" target="_blank">
        <?php if(get_option('pr_images_layout') == 'Horizontal Layout'){ ?>
            <?php if( $links[0] ) {?>
                <img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>&w=<?php echo get_option('pr_large_width'); ?>&h=<?php echo get_option('pr_large_height'); ?>" alt="single_review" />
            <?php } else { ?>
                <img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo get_template_directory_uri();?>/images/noimg-280.png&w=<?php echo get_option('pr_large_width'); ?>&h=<?php echo get_option('pr_large_height'); ?>" alt="single_review" /><?php } ?>
        <?php } else { ?>
        <?php if( $links[0] ) {?>
            <img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>&w=<?php echo get_option('pr_large_width'); ?>&h=<?php echo get_option('pr_large_height'); ?>" alt="single_review" />
        <?php } else { ?>
            <img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo get_template_directory_uri();?>/images/noimg-280.png&w=<?php echo get_option('pr_large_width'); ?>&h=<?php echo get_option('pr_large_height'); ?>" alt="single_review" /><?php } ?>
        <?php } ?>
      </a>
    </div> <!--end preload-->
    <div class="review-info">
        <p class="review-info-title"><?php the_title(); ?></p>
        <span class="review-info-rating">
        <?php
            $ar = average_rating(); {
            if($ar == '0') {
              echo '<div class="average_rating stars_0"><?php echo average_rating(); ?></div>     ' ;
            }       
            elseif($ar == '1') {
              echo '<div class="average_rating stars_1"><?php echo average_rating(); ?></div>' ;
            } 
            elseif($ar == '1.5') {
              echo '<div class="average_rating stars_1_5"><?php echo average_rating(); ?></div>   ' ;
            }   
            elseif($ar == '2') {
              echo '<div class="average_rating stars_2"><?php echo average_rating(); ?></div>     ' ;
            } 
            elseif($ar == '2.5') {
              echo '<div class="average_rating stars_2_5"><?php echo average_rating(); ?></div>   ' ;
            } 
            elseif($ar == '3') {
              echo '<div class="average_rating stars_3"><?php echo average_rating(); ?></div>     ' ;
            } 
            elseif($ar == '3.5') {
              echo '<div class="average_rating stars_3_5"><?php echo average_rating(); ?></div>   ' ;
            } 
            elseif($ar == '4') {
              echo '<div class="average_rating stars_4"><?php echo average_rating(); ?></div>     ' ;
            } 
            elseif($ar == '4.5') {
              echo '<div class="average_rating stars_4_5"><?php echo average_rating(); ?></div>   ' ;
            } 
            elseif($ar == '5') {
              echo '<div class="average_rating stars_5"><?php echo average_rating(); ?></div>     ' ;
            } 

            }
            ?>
            &nbsp;&nbsp;  
            <span>
                <?php 
                    echo $ar.' ';
                    ($ar > 1) ? _e(" Ratings from ",'prt') : _e(" Rating from ",'prt') 
                ?>
                <?php echo get_comments_number(); ?>
            </span><?php (get_comments_number() > 1) ? _e(" Reviews",'prt') : _e(" Review",'prt');?>
        </span>
    </div>
  </div>

  <div id="primary" <?php if(get_option('pr_sr_ly')=='true'){echo 'class="full_width"'; } ?>>
    
    <div class="review_single">
      <div class="review_bar <?php if(get_option('pr_sr_ly')=='true'){echo 'extra_width'; } ?>">
        <h2><?php echo $meta['r_title']; ?></h2>
        <div class="review_rating stars_<?php echo $meta['r_rating']; ?>"><?php echo $meta['r_rating']; ?>stars</div>
      </div> <!--end review_bar-->
      
      <?php if(!$meta['r_overview']) : ?>
      <div class="brief">
        <p class="meta"><?php echo get_the_term_list( get_the_ID(), 'review-cats', '', ', ', '' ) ?>
	<div>
	</div>
        </p>
        <?php if($meta['r_price']) : ?>
        <p class="price"><?php _e("Price",'prt');?>: <?php echo $meta['r_price']; ?></p>
        <?php endif; ?>
        <p><?php echo $meta['r_description']; ?></p>
        <a href="<?php echo $meta['r_aff_link']; ?>" class="visit" rel="nofollow" target="_blank">00000</a> </div>
      <!--end brief-->
      <div class="clear"></div>
      <?php endif; ?>
	  
<div id="average_rating_wrap">
<div id="editor-rating">
        <div class="review_rating stars_<?php echo $meta['r_rating']; ?>"><?php echo $meta['r_rating']; ?>stars</div>
	<br><br>
	<p class="user-rev"> <?php _e("Editors rating",'prt');?></p>
</div>
</div>
      <?php the_content(); ?>
      <?php if($meta2){?>
      <div class="conclusion_table">
        <div class="table_top"> <span class="cl">Conclusion</span> <span class="rat">Rating</span> </div> <!--end table_top-->
        <?php foreach ( $meta2['c_group'] as $conclusion ) { ?>
        <div class="t_row">
          <div class="table_left"> <?php echo $conclusion['c_title']; ?> </div> <!--end table_left-->
          <div class="table_right review_rating stars_<?php echo $conclusion['c_rating']; ?>"><?php echo $conclusion['c_rating']; ?> stars </div> <!--end table_right-->
        </div> <!--end t_row-->
        <?php } ?>
      </div> <!--end conclusion table-->
      <?php  } ?>
	<?php link_pages('<p><strong>Pages:</strong>', '</p>', 'number'); ?>		  
	<?php $review_tags= get_the_term_list( get_the_ID(), 'review-tags', '', ', ', '' ); if($review_tags): ?>
      <p class="meta tags"><?php _e("Tags :", 'prt'); ?>  <?php echo $review_tags;  ?> </p>
      <?php endif; ?>
    </div> <!--end review_single-->
    <div class="reviews_pagination">
<div class="page_navigation">
            <div id="more_prev">
            	<div class="newer_links"><?php previous_post_link( '%link', __( __("Older Review", 'prt') ) ); ?></div>
            	<div class="previous_link"><?php next_post_link( '%link', __( __("Newer Review", 'prt' ) ) ); ?></div>
             </div><!--end more_prev -->
</div>

    </div> <!--end reviews_pagination-->
	
    <?php  endwhile; endif; ?>
    <div id="comments_template">
      <?php comments_template(); ?>
    </div>
  </div> <!--end primary-->
  <?php if(get_option('pr_sr_ly')!='true'){ get_sidebar(); } ?>
</div> <!--end main_content-->
<?php get_footer(); ?>
